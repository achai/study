# table of content

1.download boxes and installation  
2.centos configuration  
3.installation for git  
3.installation for vim
4.instalatiion for oracle  


## download boxes and installation
download and install centos7 for oracle.
this box is configured for oracle..

```
vagrant box add centos/7
```



## installation for git
yum install git packages and required packages
```
sudo yum install git
```

And you needs to configure vim settings.


# installing for vim  
this box does not contain vim command.
To edit config files,it is necessary vim command.

```
sudo yum install vim
```

## 3.installing oracle database  

you finished download oracle database rpm packages.  
And you need to share oracle package files. between hostpc(mac) and virtual host(vagrant).
Oracle package files put in shared direcory.

```
rpm -ivh oracle-xe-11.2.0-1.0.x86_64.rpm
```

you finished installation.
you needs to configure settings.

  -
```
/etc/init.d/oracle-xe configure
```

### oracle user and set environment variables.

I change a password on oracle user.

```
sudo passwd oracle
```

I set enviromental variables in `/etc/profiles`.

```
export ORACLE_BASE=/u01/app/oracle
export LD_LIBRARY_PATH=$ORACLE_HOME/lib/:/lib:/usr/lib
export NLS_LANG=Japanese_Japan.AL32UTF8
export NLS_DATE_FORMAT='YYYY/MM/DD HH24:MI:SS'
```

I establish connection on oracledatabase.

```
sqlplus /nolog
```

I completed installing for oracle.

# reference url
[intalled box information and configuration](https://seven.centos.org/2016/12/updated-centos-vagrant-images-available-v1611-01/)


[Installing for oralce xe](https://docs.oracle.com/cd/E17781_01/install.112/e18802/toc.htm#XEINL122)
