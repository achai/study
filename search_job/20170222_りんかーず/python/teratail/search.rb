require 'kconv'

INDEX_FILE = 'index.txt'.freeze
CSV_FILE = 'KEN_ALL.CSV'.freeze
CSV = File.readlines(CSV_FILE, encoding: 'SJIS')
          .map { |row| row.encode('utf-8') }
          .freeze

def match_strs(address, substrs)
  substrs.each { |str| return false unless address.include?(str) }
  true
end

def search(substrs)
  File.open(INDEX_FILE, 'r') do |file|
    file.each_line do |line|
      strs = line.split(' ')
      idx = strs[0].to_i
      address = strs[1..-1].join
      puts CSV[idx] if match_strs(address, substrs)
    end
  end
end

substrs = ARGV
search(substrs)
