# coding: utf-8
# See http://qiita.com/ya-mada/items/c162383eda33dc516c39
#     http://tech-kazuhisa.hatenablog.com/entry/20121110/1352558835
#     http://seesaawiki.jp/emanon/d/Ruby%A4%C7%CD%B9%CA%D8%C8%D6%B9%E6%A4%F2%B8%A1%BA%F7

require 'zip'
require 'csv'
require 'kconv'

# Zip 解凍
def uncompress(path, outpath)
  entrys = []
  Dir.mkdir(outpath) unless Dir.exist?(outpath)
  # 2つ目の引数は offset
  Zip::InputStream.open(path, 0) do |input|
    # get_next_entryすると input の offset（ポインタ）が動く
    while (entry = input.get_next_entry)
      # 書き出し先を作る
      save_path = File.join(outpath, entry.name)
      File.open(save_path, 'w') do |wf|
        # get_next_entry でポインタが動いているので、毎回 input.read で OK
        wf.puts(input.read)
      end
      entrys << save_path
    end
  end
  # 解凍されたファイル名の配列を返す
  entrys
end

def make_index(csv_filename, address_file)
  File.open(address_file) do |file|
    CSV.foreach(csv_filename, encoding: 'SJIS:UTF-8').with_index do |row, idx|
      output_record = "#{idx} #{row[6]} #{row[7]} #{row[8]}"
      file.puts(output_record.encode('utf-8'))
    end
  end
end

ZIP_FILE = 'ken_all.zip'.freeze
UNZIP_OUT_DIR = '.'.freeze
ADDRESS_FILE = 'index.txt'.freeze

filenames = uncompress(ZIP_FILE, UNZIP_OUT_DIR)
make_index(filenames[0], ADDRESS_FILE)
