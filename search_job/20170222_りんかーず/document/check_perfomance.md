self.map.with_index{|item, i| i if item.include?(value)}.compact

item：特異メソッド（一時的な）配列を作る
include?(value):文字列が含まれているかチェック
with_index：生成時のパラメータに従って、要素にインデックスを添えて繰り返します。 インデックスは offset から始まります。


渋谷という文言が含まれるは多分5250レコード

[root@calc-eve1 rb]# grep "渋谷" index_file.csv | wc -c
5250

で、プログラムで実行すると
はずかしい。行数カウントするオプションはl(line)である。

次のことで正しいことをテストする。
- grepコマンド
- wc -l の行数


```
[root@calc-eve1 rb]# wc -l log.txt
85 log.txt```
```

うーん、うまくいかない。原因分析だ！！！
```
[root@calc-eve1 rb]# grep "渋谷"  index_file.csv | wc -l
84
```
ん？？１行足りない？？まぁ、一旦はOKとするかー
