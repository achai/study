★お手もとの環境溶離も新しいカーネルが入っているメディア。
　リポジトリとして登録すると、# yum update kernel が試せます。


�@ /etc/yum.repos.d/xxxxxx.repo の編集
[rhel71-base-media]
name=RHEL71-base-dvd
baseurl=http://192.168.5.70/rhel71-base-media
enabled=1
gpgcheck=0

もしくは

�Ayum-config-managerを使用する場合
# yum-config-manager --add-repo=http://192.168.5.70/rhel71-base-media
# vi 
⇒ "gpgcheck=0" を追記。
     システムグローバルな設定として、/etc/yum.confに、gpgcheck=1の設定が入っているため、
　　受験環境のように信頼できるリポジトリなら、個別の/etc/yum.repos.d/xxx.repoに"=0"の設定を入れる必要があります。