# CSS反映方法
***
HTMLファイルにスタイルシートを反映する方法は３つある。

- タグの中に記載する。
（<h1 style=color; blue:）
- headタグに記載。
- cssファイルを外部ファイルとして、htmlに取り込む。にリンクする。
  htmlとcssは役割が違うので分けるのが基本。また各ファイルは複数に増やすことは元

タグ内にCSSを入れる場合、システム的に必要という例外的な対応と考えたほうがいい。

## classとidを使う
---
使い分け：idは一回しか使わないので、classのみを使う方がよい。
javascriptsでidを使わない。

- index.html   
`<h1 class="class">test</h1>   
<h1 id="id">test</h1>`

- test.css  
`h1.class{}`
`#id{}`

# CSSの上書き
---
同じセレクタが複数存在した場合、後ろにあるもの適用される。

`p.class{
    color: blue;
    width; auto;
    height; 100px;
}
p.class{
	color: red;
	font-size: 12px;
	font-family: sans-serif;
}`

# 画像を半透明にする方法   
---   
ここのサイトを参考  
https://www.xenophy.com/webdesign/4295



# 画像を横並び
# 質問事項  
課題をやっている最中に思った質問事項をまとめる。   

Q1:「align」プロパティはCSS内では反映されない？
