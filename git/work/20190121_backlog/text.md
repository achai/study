# 概要
BACKLOGのgitチュートリアル

#
いつ、どこで、なんのためといったものを管理するためのバージョン管理システム。


```

```

プログラムだけなく、書籍（小説）でもGITで管理する人もいる。
GITに保存していれば、必ずそこまで戻れる。


```
mkdir test


# initraze git
git init
```



レポジトリ作成。（ディレクトリ）


```
vim hoge.txt

test
```

add staging(index erea)
```
git add test

```

modify content and confirm differcce

```
asaitaksMacBook:test asaitakayuki$ git diff
diff --git a/test.txt b/test.txt
index 9daeafb..b06e02b 100644
--- a/test.txt
+++ b/test.txt
@@ -1 +1,2 @@
 test
+add record
```

add a file



rm a copy_test.txt


```
asaitaksMacBook:test asaitakayuki$ git status
On branch master
Changes not staged for commit:
  (use "git add/rm <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	deleted:    test_copy.txt

no changes added to commit (use "git add" and/or "git commit -a")
```

 プッシュ：ローカル上と変更情報（メタ情報）。
 プル：リモートレポジトリからレポジトリを取得する。

はじめてのときは、cloneする。ローカルとサーバが紐付いているので、



# ブランチ
ブランチはひとつしか選べない。また、作業中のファイルがあるとブランチは
ブランチをプッシュする。
ブランチをきることで、他のブランチに影響するすることなく、進めることができる。
```
git push origin asai
```


```
asaitaksMacBook:backlogworld asaitakayuki$ git fetch
remote: Counting objects: 11, done.
remote: Compressing objects: 100% (10/10), done.
remote: Total 11 (delta 4), reused 0 (delta 0)
Unpacking objects: 100% (11/11), done.
From https://meganedoti.backlog.com/git/BACKLOGWORLD2019/backlogworld
 * [new branch]      gotoaki        -> origin/gotoaki
 * [new branch]      hayashidaisuke -> origin/hayashidaisuke
 * [new branch]      kudou          -> origin/kudou
   726441c..0604ad5  master         -> origin/master
 * [new branch]      megane         -> origin/megane
asaitaksMacBook:backlogworld asaitakayuki$
asaitaksMacBook:backlogworld asaitakayuki$
asaitaksMacBook:backlogworld asaitakayuki$ git branch
* asai
  master
```

リモート側のブランチをみる。
```
git branch -a

asaitaksMacBook:backlogworld asaitakayuki$ git branch -a
* asai
  master
  remotes/origin/asai
  remotes/origin/gotoaki
  remotes/origin/hayashidaisuke
  remotes/origin/kudou
  remotes/origin/master
  remotes/origin/megane
```


ブランチを切り替えて、プルする。

```
saitaksMacBook:backlogworld asaitakayuki$ git checkout master
Switched to branch 'master'
Your branch is behind 'origin/master' by 13 commits, and can be fast-forwarded.
  (use "git pull" to update your local branch)
```
他の人のブランチがみられる。

```
asaitaksMacBook:backlogworld asaitakayuki$ git pull
remote: Counting objects: 3, done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 3 (delta 1), reused 0 (delta 0)
Unpacking objects: 100% (3/3), done.
From https://meganedoti.backlog.com/git/BACKLOGWORLD2019/backlogworld
   938c3de..94a9209  master     -> origin/master
Updating 726441c..94a9209
Fast-forward
 asai.txt     | 1 +
 gotoaki.txt  | 1 +
 hayashi.txt  | 1 +
 key.txt      | 1 +
 key_test.txt | 1 +
 kudou.txt    | 1 +
 megane.txt   | 4 ++++
 7 files changed, 10 insertions(+)
 create mode 100644 asai.txt
 create mode 100644 gotoaki.txt
 create mode 100644 hayashi.txt
 create mode 100644 key.txt
 create mode 100644 key_test.txt
 create mode 100644 kudou.txt
 create mode 100644 megane.txt

```


バックログから
# 質問
分散型と集中型
ファイルを削除した場合は、どのように指定する？？
baklogのgitとgithubは何が違う？？
