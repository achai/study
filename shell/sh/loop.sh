#!/bin/sh

#
# YYYY-MM-DD
#
STARTDATE=2015-02-20
ENDDATE=2015-02-25
CURRENTDATE=$STARTDATE

while [ 1 ] ; do

        # 処理
        echo $CURRENTDATE

        if [ "$CURRENTDATE" = "$ENDDATE" ] ; then
                break
        fi

         CURRENTDATE=`date -d "$CURRENTDATE 1day" +"%Y-%m-%d"`

done
