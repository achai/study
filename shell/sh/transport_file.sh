#!/bin/bash

# BEGIN:YYMM END:YYMM
BEGIN_YEAR=
BEGIN_MONTH=
END_YEAR=
END_MONTH=

echo "$BEGIN_YEAR"
echo "$BEGIN_MONTH"
echo "$END_YEAR"
echo "$END_MONTH"

# 同じ年の場合

## 月の差分のみ

# 一年以上の場合

# CURRENT=$BEGIN
#
# # 開始日〜終了日までループ
# while true; do
#     # ここに日付毎の処理を書く
#     echo "処理: $CURRENT"
#
#     if [ "$CURRENT" = "$END" ]
#     then
#     break
#     fi
#
#     # 日付を1日インクリメント
#     CURRENT=$(date -d "$CURRENT 1day" "+%Y%m%d")
# done
