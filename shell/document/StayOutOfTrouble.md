# overview
I learn how to debagge in shellscirpts.

# sample code

```

```trouble.sh
#!/bin/bash

#declear variables
number=1

if [ number = 1 ] then
  echo " number values is 1"
elif
  echo " number values is not 1"
fi
```

executeion result
```
trouble.sh: line 6: [: =: unary operator expected
 number values is not 1
````

the =(eqaul) is binary operator.
（イコールはバイナリー演算子である。）

# watching your script
there are two ways to trac your running script.

  - use x option.
  - alternative you can turn tracing on set -x,toff set x.

## exmaple code
### usging x option
x option is included shell.
when execute shell scirpt,launch bash scirpt with x option.

#### syntax

```
/bin/bash -x {shell_script_name}
```
other way to
```
#!/bin/bash -x
```

#!/vin/bash -x

{ code}
```

or

```

#turn tracing on
set -x

{code}

#turn tracing off
set +x

```


# reference url
text
http://linuxcommand.org/lc3_wss0090.php
