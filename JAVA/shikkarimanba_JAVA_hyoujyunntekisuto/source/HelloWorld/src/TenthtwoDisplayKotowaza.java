import java.io.*;

public class TenthtwoDisplayKotowaza {
	public static void main(String args[]) throws IOException{
		//declear variable
		String su;
		int number;
		
		//Display Message
		System.out.println("番号（学生：１、社会人：２、その他：３）を入力");
		BufferedReader abc = new BufferedReader(new InputStreamReader(System.in));
		
		su = abc.readLine();
		number = Integer.parseInt(su);
		
		//Display Message 
		if( number == 1 )
		{
			System.out.println("学びて思わざれば即ちくらし");
		}
		else if(number == 2)
		{
			System.out.println("人一たびして之を能くすれば己之を百たびす");
		}
		else
		{
			System.out.println("年んを問わんより世を問え");
		}
	}	
}
