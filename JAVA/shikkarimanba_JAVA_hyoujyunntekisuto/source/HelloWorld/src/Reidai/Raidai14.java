package Reidai;
import java.io.*;

public class Raidai14 {
	public static void main(String args[]) throws IOException {
        //declare variable
		int n,AnyNumber,Sum = 0;
		String s;

		//display message and input any number. 
		System.out.println("I sum number from one to any number");
		BufferedReader abc = new BufferedReader(new InputStreamReader(System.in));
		s = abc.readLine();
		AnyNumber = Integer.parseInt(s);
		
        //calculate sum.
        for( n =1; n <= AnyNumber ; n++){
        	Sum = n + Sum;
        }
        
        //Display sum.
        System.out.println("1から" + AnyNumber + "までの合計は" + Sum + "です" );
	}
}