package Reidai;

public class Test {
	public static void main(String[] args) {
		//変数初期化
		int StartValue;
		int EndValue;
		int Zoubun;
		
		StartValue = 1;
		EndValue = 10;
		Zoubun = 10;
		
		//開始値
		System.out.println("開始値：");
		StartValue = Integer.parseInt(args[0]);
		//終了値
		System.out.println("終了値：");
		EndValue = Integer.parseInt(args[1]);
		//増分
		System.out.println("増分：");
		Zoubun = Integer.parseInt(args[2]);
		//計算
		printSequenceOfSum(StartValue, EndValue, Zoubun);
}
		static void printSequenceOfSum(int StartValue, int EndValue, int Zoubun) {
			int OutPutCount;
			
			//等差数列を表示
			for(int i = StartValue; i <= EndValue; i += Zoubun) {
				System.out.printf("%3d", i);
				//カンター増分
				++OutPutCount;
				//10個表示された時、改行
				if((OutPutCount % 10 == 0)) {
					System.out.println("\n");
				}
			}
		}
}