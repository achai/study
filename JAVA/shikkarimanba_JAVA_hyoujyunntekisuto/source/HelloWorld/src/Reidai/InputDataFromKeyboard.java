package Reidai;
import java.io.*;

public class InputDataFromKeyboard {
	public static void main(String[] args) throws IOException {
		//変数初期化
		String name;
		
		//名前入力プロンプト表示し、変数に格納
		System.out.println("あなたの名前は？");
		BufferedReader abc = new BufferedReader(new InputStreamReader(System.in));
		name = abc.readLine();

		//名前表示		
		System.out.println("こんにちは" + name);
		System.out.println( name + "はJAVAを学んでおります。");
		
	}
}
