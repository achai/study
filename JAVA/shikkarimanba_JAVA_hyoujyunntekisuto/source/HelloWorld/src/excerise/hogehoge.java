package excerise;

public class CalclationOfTime {

//package Member;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Map;
import java.io.*;

class MemberSystem implements Serializable
{
    private static final long serialVersionUID = 1L;

    private static SortedMap<String,MemberSystem> mbrList = new TreeMap<String,MemberSystem>();

    static final String MEMBER_FORMAT = "JAVA%04d";
    static final String MEMBER_FILE = "member.dat";

    private String no;
    private String name;
    private String birth;
    private String tel;

    public MemberSystem() {
    }

    public static void main(String[] args)
    {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        String str = "";
        boolean end = false;
        try {
            while (!end) {
                System.out.println("◆メニュー番号を選択してください");
                System.out.println("1:登録 2:照会 3:検索 4:削除 5:保存 6:読込 8:HELP 9:終了");
                str = br.readLine();
                int n = Integer.parseInt(str);
                System.out.println("メニュー番号＞");
                switch (n) {
                    case 1:
                    System.out.println("会員情報を登録します。");
                    add_member(br);
                    break;
                case 2:
                    System.out.println("会員情報を照会します");
                    query_member(br);
                    break;
                case 3:
                    System.out.println("会員情報を検索します。");
                    search_member(br);
                    break;
                case 4:
                    System.out.println("会員情報を削除します。");
                    del_member(br);
                    break;
                case 5:
                    System.out.println("会員情報を保存します。");
                    save_member();
                    break;
                case 6:
                    System.out.println("会員情報を読み込みます。");
                    load_member();
                    break;
                case 8:
                    System.out.println("会員管理システムの使い方を表示します。");
                    break;
                case 9:
                    System.out.println("####会員管理システムを終了します。お疲れ様でした。####");
                    end = true;
                    break;
                default:
                    System.out.println("1～9の範囲でもう一度番号入力をして下さい。");
                    break;
                }
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    static void add_member(BufferedReader br) throws IOException {
        try {
            System.out.println("■名前を入力して下さい  例）佐藤花子");
            String name = br.readLine();

            System.out.println("■生年月日を入力して下さい 例）19960130");
            String birth = br.readLine();

            System.out.println("■電話番号を入力して下さい 例）0363925367");
            String tel = br.readLine();

            MemberSystem mbr = new MemberSystem();
            mbr.no = String.format(MEMBER_FORMAT, mbrList.values().size() + 1);
            mbr.name = name;
            mbr.birth = birth;
            mbr.tel = tel;

            mbrList.put(mbr.no, mbr);
            System.out.println("会員登録が完了しました。");
       }
       catch (IOException e) {
         throw e;
       }
    }    

    static void del_member(BufferedReader br) throws IOException {
        try {
            System.out.println("削除する会員のIDを入力してください。例）DGS0001>");
            String no = br.readLine();
            MemberSystem mbr = mbrList.remove(no);
            if (null == mbr) {
                System.out.println("指定した会員IDは見つかりません。");
            }
            else {
                System.out.println("削除しました。");
            }
        }
        catch (IOException e) {
            throw e;
        }
    }

    static void query_member(BufferedReader br) throws IOException {
        try {
            System.out.println("表示する会員のIDを入力してください。指定なしで全件表示。例）DGS0001>");
            String no = br.readLine();
            MemberSystem mbr = mbrList.get(no);
            if (null == mbr) {
                System.out.println("指定した会員IDは見つかりません。");
            }
            else {
                System.out.println("会員番号: " + mbr.no);
                System.out.println("氏名    : " + mbr.name);
                System.out.println("生年月日: " + mbr.birth);
                System.out.println("電話番号: " + mbr.tel);
            }
        }
        catch (IOException e) {
            throw e;
        }
    }

    static void search_member(BufferedReader br) throws IOException {
        try {
            System.out.println("表示する会員の名前を入力してください。指定なしで全件表示。例）佐藤花子＞");
            String key = br.readLine();

            if (key.length() == 0) {
                for (Map.Entry<String,MemberSystem> entry : mbrList.entrySet()) {
                    show(entry.getValue());
                }
            }
            else {
                for (Map.Entry<String,MemberSystem> entry : mbrList.entrySet()) {
                    if (key.equals(entry.getValue().name)) {
                        show(entry.getValue());
                    }
                }
            }
        }
        catch (IOException e) {
            throw e;
        }
    }

    static void show(MemberSystem mbr) {
        System.out.println("会員番号: " + mbr.no);
        System.out.println("氏名    : " + mbr.name);
        System.out.println("生年月日: " + mbr.birth);
        System.out.println("電話番号: " + mbr.tel);
        System.out.println("----");
    }

    static void save_member() throws IOException {
        try {
            ObjectOutput out=new ObjectOutputStream(new FileOutputStream(MEMBER_FILE));
            out.writeObject(mbrList);
            out.flush();
            out.close();
            System.out.println("会員情報を保存しました。");
        }
        catch (IOException e) {
          throw e;
        }
    }

    static void load_member() throws IOException, Exception {
        try {
            ObjectInputStream in=new ObjectInputStream(new FileInputStream(MEMBER_FILE));
            mbrList = (TreeMap<String,MemberSystem>) in.readObject();
            in.close();
            System.out.println("会員情報を読み込みました。");
        }
        catch (IOException e) {
          throw e;
        }
        catch (Exception e) {
          throw e;
        }
    }
}