package excerise;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class SixtheenCalclationSalary {
	public static void main(String args[]) throws IOException {
		//declare variable
		int SalaryPerHour = 850;
		String Temp1;
		int WorkingTime,Salary;
		//
		System.out.println("パートタイマーの勤務時間は？");
		BufferedReader abc = new BufferedReader(new InputStreamReader(System.in));

		//get values
		Temp1 = abc.readLine();
		
		//cast from string to number
		WorkingTime = Integer.parseInt(Temp1);
		Salary = WorkingTime * SalaryPerHour;
		
		//DisplayMessage
		System.out.println("時給は" + SalaryPerHour  + "円で勤務時間" + WorkingTime + "時間");
		System.out.println("給料は" + Salary + "円です。");
	}
}
