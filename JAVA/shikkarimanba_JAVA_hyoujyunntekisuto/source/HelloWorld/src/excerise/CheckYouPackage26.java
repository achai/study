package excerise;
import java.io.*;

public class CheckYouPackage26 {
	public static void main(String args[]) throws IOException {
		//
		String s;
		
		//input data from keyboard
		System.out.println("調べる種類とサイズの記号（BS,BM,BL,PS,PL）を半角英大文字で入力");
		BufferedReader abc = new BufferedReader(new InputStreamReader(System.in));
		s = abc.readLine();
		
		if( s.equals("BS")) {
			System.out.println("大きさ:23✕17✕11cm");
			System.out.println("料金：100円");			
		}else if ( s.equals("BM")) {
			System.out.println("大きさ:32✕23✕15cm");
			System.out.println("料金：140円");			
		}else {
			System.out.println("入力した英字が間違っています。");
		}
	}
}