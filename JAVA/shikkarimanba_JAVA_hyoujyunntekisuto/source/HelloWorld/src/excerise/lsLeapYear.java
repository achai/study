package excerise;

public class lsLeapYear {
	    public static void main( String args[] ) {
	        int year = 1987;
	        
	        if( year % 4 == 0 && year % 100 != 0 || year % 400 == 0) {
	            System.out.println( year + "年はうるう年です" );
	        } else {
	            System.out.println( year + "年は平年です" );
	        }
	    }
}