package excerise;
import java.io.*;

public class FortheenInputData {
	public static void main(String[] args) throws IOException {
		//initiaize variable
		String name;
		
		//Promote and input name
		System.out.println("あなたの名前は？");
		BufferedReader abc = new BufferedReader(new InputStreamReader(System.in));
		name = abc.readLine();
		
		//Display Message
		System.out.println(name + "が入力されました。");
		System.out.println("私の名前は" + name + "です。");
		System.out.println( name + "をよろしくお願いいたします。");
	}
}
