package excerise;
import java.io.*;

public class ForStatement29 {
	public static void main(String args[]) throws IOException{
		String s;
		int n,count;
		
		System.out.println("please enter the number of time");
		BufferedReader abc = new BufferedReader(new InputStreamReader(System.in));
		s = abc.readLine();
		count = Integer.parseInt(s);
		
		for( n = 1; n <= count; n++) {
			System.out.println( n );
		}
	}
}
