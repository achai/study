import java.io.*;

public class Lesson4_1 {
	public static void main(String args[]) throws IOException{
		String suchi;
		double hankei, enshu, menseki;
		double pai = 3.14;
		
		System.out.println("円の半径(cm)を入力");
		
		BufferedReader abc = new BufferedReader(new InputStreamReader(System.in));
		
		suchi = abc.readLine();
		
		hankei = Double.parseDouble(suchi);
		enshu = 2.0 * pai * hankei;
		menseki = pai * hankei * hankei;
		System.out.println("半径" + hankei + "cmのとき");
		System.out.println("円周 = " + enshu + "cm");
		System.out.println("面積 = " + menseki + "平方cm");
	}
}
