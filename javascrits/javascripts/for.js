$(function() {
  $.getJSON("data.json" , function(data) {
    var
      ulObj = $("#json"),
      len = data.length;
    for(var i = 0; i < len; i++) {
      ulObj.append($("<li>").attr({"id":data[i].id}).text(data[i].name));
    }
 });
});