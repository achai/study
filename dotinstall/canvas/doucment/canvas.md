# 05 線や塗りのスタイルを変えてみよう   
- strokeStyle:線の色（RGB,RED,bule）
- fillStyle: 塗り潰しの色
- lineWidth: 線の幅
- linejoin:  線のぶつかった時の

# 06 グラデーションを設定してみよう

- 
    - createLinearGradient（x座標,y座標,）：グラデーション
    - addColorStop:グラデーションの色と場所を指定

-　線形：
-　円形：radial


# 07 影と透明度の設定をしてみよう
影をつける。

- shadowColor:色
- shadowOffsetx:x軸方向のずれ具合
- shadowOffsety:y軸方向のずれ具合
- shadowBlur:影の長さ
- golobalAlpha:透明度

#　08 図形を変形させてみよう    
変形するときは、描写する前に行う。

- translate: x方向への移動
- lotate:図の回転

#　09 線を描いてみよう   

(object).beginPath();線引き開始  
(object).moveTo(x,y);開始位置   
(object).lineTo(x,y);終了位置   
(object).closePath();線引き終了   
 
# 10 曲線を描いてみよう

- arc(x座標,y座標,半径,開始位置、終了位置):曲線の描写