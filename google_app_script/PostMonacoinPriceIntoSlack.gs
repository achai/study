var postUrl = "https://hooks.slack.com/services/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"; // incomng webhookのPost ID
var postChannel = "#get_monacoin_currency"; // Channel
var username = 'ZaifAPI'; // ユーザー名(表示名)
var message = 'current monacoin price: ';
var unit = ' (mona)';

function GetmonaCrrencyPrice() {
  /***********************************************
  set variable
  get monacoin price by zaif api.
  ***********************************************/
  var json_price = UrlFetchApp.fetch("https://api.zaif.jp/api/1/last_price/mona_jpy");
  var price = JSON.parse(json_price)["last_price"]

  /***********************************************
  set variable
  get monacoin price by zaif api.
  ***********************************************/
  var jsonData =
  {
     "channel" : postChannel,
     "username" : username,
     "text" :  message + price + unit
  };
  var payload = JSON.stringify(jsonData);
  var options =
  {
    "method" : "post",
    "contentType" : "application/json",
    "payload" : payload
  };

  UrlFetchApp.fetch(postUrl, options);
}
