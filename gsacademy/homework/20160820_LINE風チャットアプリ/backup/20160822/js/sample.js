var Messages = new Meteor.Collection('messages');
 
if (Meteor.isClient) {
 
  var img = [
    'img/hato1.png','img/hato2.png','img/hato3.png',
    'img/hato4.png','img/hato5.png','img/hato6.png','img/hato7.png'];
  var num = Math.floor(Math.random() * img.length);
  var usrimg = img[num];
 
  var maxrec = 10;
 
  Template.contents.helpers({
    maxrec: maxrec,
    usrimg: usrimg,
    messages : function () {
      return Messages.find({},{sort:{date:-1}})
    }
  });
 
  Template.contents.events({
    'click .submit-btn' : function(event){
      var message = $('#postmes').val();
      if(message != ''){
        $('#postmes').val('');
        var date = Date.parse(new Date());
        var datetime = toDateStr(date);
        var name = $('#postname').val();
        var img = usrimg;
        if(name == ''){
          name = 'guest';
        }
 
      // コレクションへ新レコードを登録
      Messages.insert({
        date : date,
        datetime : datetime,
        message : message,
        name : name,
        img : img
        },function(err,_id){
          var len = Messages.find({}).fetch().length;
          if(len &gt; maxrec){
            var doc = Messages.findOne({}); 
            Messages.remove({_id:doc._id}); 
          }
        });
      }
    }
  });
 
  // 1桁の数字を0埋めで2桁にする
  var toDoubleDigits = function(num) {
    num += &quot;&quot;;
    if (num.length === 1) {
      num = &quot;0&quot; + num;
    }
   return num;
  };
 
  // 日付を文字列に変換
  var toDateStr = function(parseDate){
    var date = new Date(parseDate);
    var y = toDoubleDigits(date.getFullYear());
    var m = toDoubleDigits(date.getMonth()+1);
    var d = toDoubleDigits(date.getDate());
    var h = toDoubleDigits(date.getHours());
    var min = toDoubleDigits(date.getMinutes());
    return y+&quot;/&quot;+m+&quot;/&quot;+d+&quot; &quot;+h+&quot;:&quot;+min;
  }
 
}
 
if (Meteor.isServer) {
}