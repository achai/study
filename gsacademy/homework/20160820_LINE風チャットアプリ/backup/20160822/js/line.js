$(function(){

    //データストアを作成（データを送受信するために必要）
    var milkcocoa = new MilkCocoa('juiceis2reodd.mlkcca.com');//※ID自分のに変えてね！！

    //データベースストア：オブジェクト生成
    var chatDataStore = milkcocoa.dataStore('chat');

    //送信処理（ボタンをクリック）
    $("#button").on("click",function(){
        var name = "asai"
        var text = $("#msg").val();//入力文字を取得
        var d = new Date();//オブジェクト生成
        var year  = d.getFullYear();
        var month = d.getMonth() + 1;
        var day   = d.getDate();
        var hour  = ( d.getHours()   < 10 ) ? '0' + d.getHours()   : d.getHours();
        var min   = ( d.getMinutes() < 10 ) ? '0' + d.getMinutes() : d.getMinutes();
        var timestamp =(year + '-' + month + '-' + day + ' ' + hour + ':' + min);
        //milkcocoa:push送信
        chatDataStore.push({
                //送信したい名前と値
                name:name,message:text,timestamp:timestamp
            },function(e){
                //送信後処理
                $("#msg").val("");
            }
        );
    });

    //受信処理
    chatDataStore.on("push",function(data){
        console.dir(data);
        $("#board").append("<li>"+data.value.name+":"+data.value.timestamp+":"+data.value.message+"</li>");
        $('#board').animate({scrollTop: $('#board')[0].scrollHeight}, 'fast');
    });

    var history = milkcocoa.dataStore('chat').history();
    history.sort('desc'); //ASC昇順、DESC降順
    history.size(1);
    history.limit(10);
    var i = 0;
    history.on('data', function(data) {
          data.forEach(function(d,i){
            $("#board").append("<li>"+data[i].value.name+":"+data[i].value.timestamp+":"+data[i].value.message+"</li>");
          });
    });
    history.on('end', function() {
        console.log('end');
    });
    history.on('error', function(err) {
        console.error(err);
    });
    history.run();
});