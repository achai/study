//PCの手をランダムで表示
function pc_hand_output() {
   var imglist = new Array(
        "img/stone.png",
        "img/scissors.png",
        "img/paper.png"
    );
    var pc_hand = Math.floor(Math.random()* imglist.length );
    var pc_hand_output = "<img src=" + imglist[pc_hand] + ">";
    document.write(pc_hand_output);
}

/*--------------------------
じゃんけんの条件をランダムに表示
 引き分け：0
 勝ち：1
 負け：2
*/--------------------------

function jyanken_condition() { 
    function command_display (){
        var jyanken_condition = Math.floor(Math.random()* 3 );
        if ( jyanken_condition == 0){
            document.write("引き分けてください");
        }else if( jyanken_condition == 1) {
            document.write("勝ってください");
        }else {
            document.write("負けてください");   
        }
      }
      command_display();
}

$(document).ready(function(){
    //ユーザの手がグーの時
    $("#gu_btn").on("click",function(){
        //条件が勝ちの時 ユーザがグー、PCがチョキ
        //条件が負けの時 ユーザがグー、PCがパー
        //条件が引き分けの時 ユーザがグー、PCがグー
        if( (jyanken_condition == 1 && pc_hand == 1) ||
            (jyanken_condition == 2 && pc_hand == 2) ||
            (jyanken_condition == 0 && pc_hand == 0)
          ){
            $("#ture_or_false").html("正解！");
           }
        else{
            $("#ture_or_false").html("不正解！");
        }
    });

    //ユーザの手がチョキの時
    $("#cho_btn").on("click",function(){
        //条件が勝ちの時 ユーザがチョキ、PCがパー
        //条件が負けの時 ユーザがチョキ、PCがグー
        //条件が引き分けの時 ユーザがチョキ、PCがチョキ
        if( (jyanken_condition == 1 && pc_hand == 2) ||
            (jyanken_condition == 2 && pc_hand == 0) ||
            (jyanken_condition == 0 && pc_hand == 1)
          ){
            $("#ture_or_false").html("正解！");
           }
        else{
            $("#ture_or_false").html("不正解！");
        }
    }        }
    });
    
    //ユーザの手がパーの時
    $("#par_btn").on("click",function(){
        //条件が勝ちの時 ユーザがパー、PCがグー
        //条件が負けの時 ユーザがパー、PCがチョキ
        //条件が引き分けの時 ユーザがパー、PCがパー
        if( (jyanken_condition == 1 && pc_hand == 0) ||
            (jyanken_condition == 2 && pc_hand == 1) ||
            (jyanken_condition == 0 && pc_hand == 2)
          ){
            $("#ture_or_false").html("正解！");
           }
        else{
            $("#ture_or_false").html("不正解！");
        }
    });