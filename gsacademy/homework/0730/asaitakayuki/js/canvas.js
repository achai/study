//初期化
var canvas_mouse_event = false; //スイッチ [ true=線を引く, false=線は引かない ]  ＊＊＊
var txy   = 10;               //iPadなどは15＋すると良いかも
var oldX  = 0;                //１つ前の座標を代入するための変数
var oldY  = 0;                //１つ前の座標を代入するための変数
var bold_line = 3;            //ラインの太さをここで指定
var color = "#f00";           //ラインの色をここで指定

//------------------------------------------------
//var can = $("#drowarea")[0]; //CanvasElement
//var context = can.getContext("2d"); //描画するための準備！
//------------------------------------------------
//上2つのスクリプトを記述します。

//canvas initial setting
var can = $("#drowarea")[0];
var context = can.getContext("2d");
    
//MouseDown：フラグをTrue
//-----------------------------------------------
//$(can).on("mousedown", function(e){
//oldX = e.offsetX;       //MOUSEDOWNしたX横座標取得
//oldY = e.offsetY - txy; //MOUSEDOWN Y高さ座標取得
//canvas_mouse_event=true;
//});
//-----------------------------------------------
//上5つのスクリプトを記述します。
$(can).on("mousedown",function(e){
    canvas_mouse_event = true;
    px = e.offsetX;
    py = e.offsetY;
    console.log(px);
    console.log(py);
})

$(can).on("mousemove", function (e){ if(canvas_mouse_event==true){
      var px = e.offsetX;
      var py = e.offsetY -txy;
      //context.strokeStyle = color;
      //context.lineWidth = bold_line;
      context.beginPath();
      context.lineJoin= "round";
      context.lineCap = "round";
      context.moveTo(oldX, oldY);
      context.lineTo(px, py);
      context.stroke();
      context.closePath();
      oldX = px;
      oldY = py;
  }
});

//
$(can).on("mouseup",function(e){
          canvas_mouse_event = false;
});

//クリアーボタンAction
//-----------------------------------------------------------------
$("#clear_btn").on("click",function (){
    context.beginPath();
    context.clearRect(0, 0, can.width, can.height);
});
//-----------------------------------------------------------------

/*ペンの色変更*/
$('#pen_color').on("change",function(){
   context.strokeStyle = $(this).val();
});

/*ペンの太さ変更*/    
$('#pen_width').change(function(){
   context.lineWidth = $(this).val(); 
});


//絵の保存
$(document).ready(function(){
   $('#save_btn').on("click",function(){
   var canvas = document.getElementById("drowarea");
   var value = canvas.toDataURL();
   //console.log(canvas);
   localStorage.setItem("image",value);
   alert("絵を保存しました。");
   });
});

/*絵の表示*/
$(document).ready(function(){
    $('#display_btn').on("click",function(){
        if(localStorage.getItem("image")){
            var image = new Image();
            image.src = localStorage.getItem("image");
            image.onload = function(){
            var drowarea = document.getElementById("drowarea");
            var g = drowarea.getContext("2d");
            g.drawImage(image,0,0);
            }
        }
    });
});