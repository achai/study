# jsonとは？   
軽量データ構造の略。（文字、数字、真偽値等を扱うことができる。）


### 例題  
配列として、名前と年齢を表示させたい場合  

[Qiita]()
    
配列として、名前と年齢を表示させたい場合（複数）  
表示させる時は、for文で回す。

    var data = [
        {"name": "浅井", "age": 28},
        {"name": "豊島", "age": 28}
    ]
    for(var i in data){
        alert('name:' + data[i].name + 'age:' + data[i].age);
    }

## 
```ruby:kobito.rb
puts 'The best app to log and share programming knowledge.'
```

[Qiita](http://www.webopixel.net/javascript/91.html "Qiita")
- 