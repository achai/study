

名前、Eメールのリストを取得
select name,e-mail from gs_an_table;

全部（※）のリストを取得
select * from gs_an_table;


条件句

idが2,3のリストを取得する。（条件句）
select * from gs_an_table where id=2 or id=3;

名前に「ジーンズ太郎」が含まれている物を取得
select * from gs_an_table where name ='ジーンズ太郎';

あいまい検索
2016年6月を含むものを抽出する。
※日付は文字列で格納されている。
select * from gs_an_table where indate like '2016-06%';

emailカラムに「gmail.com」が含まれている物を取得する。
select * from gs_an_table where email like '%gmail.com';

昇順、降順
select * from gs_an_table order by indate desc;

制限
降順でやると、最新の５つものを取得する。
select * from gs_an_table order by indate desc limit 3;
