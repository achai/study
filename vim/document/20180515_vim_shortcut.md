# overview


# shortcut

| definition | shortcut | note |
|:----:|:---:|:---:|
| move to rows | {row number} or {row number}G ||||

# reference url
[how to move row](https://qiita.com/tpywao/items/bd119f0075e40d47c88b#%E8%A1%8C%E6%96%87%E6%AE%B5%E8%90%BD%E5%8D%98%E4%BD%8D%E7%A7%BB%E5%8B%95)
