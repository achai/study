## ショートカット
- インデント削除：command + [
- インデント追加：command + ]


## パネル分割
command + kで矢印ボタンで分割ができる。
- 削除：command + w
- 移動：command + k +矢印

#  11.パッケージを導入してみよう
設定から検索または公式先とからダウンロード

## htmlプレビュー
markdown形式で記載したhtmlをプレビューしたい場合、
「control + command + m」でプレビューができる。
