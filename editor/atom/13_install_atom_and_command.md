## apmとは
アトムマネージャーの略
### オプション
- --list:インストール済みパッケージを表示
```
asaitakayuki:atom macbookair$ apm list
```
```
Built-in Atom Packages (89)
├── about@1.4.2
├── archive-view@0.61.1
├── atom-dark-syntax@0.27.0
├── atom-dark-ui@0.51.0
├── atom-light-syntax@0.28.0
├── atom-light-ui@0.43.0'
```
 - serach:インストール可能なパッケージ導入
 ```
 asaitakayuki:atom macbookair$ apm search atom-color
```
```
 Search Results For 'atom-color' (27)
 ├── atom-json-color Color a JSON in Atom Editor with a color per level (966 downloads, 12 stars)
 ├── color-tabs Adds colors to tabs.. (5890 downloads, 16 stars)
 ├── an-color-picker Atom color picker !!! (9265 downloads, 10 stars)
 ```
- install:パッケージ導入（再起動必要）
