MacでSeleniumをつかってChromeを起動するまで

### 必要なもの
 - Mac OS X
 - Google Chrome
 - [Homebrew](http://brew.sh/)
 - [Node.js](http://nodejs.org/)    
 コマンドラインからうまくいかず、インストーラーで導入
 - selenium-server-standalone
 -  chromedriver
 -  java JDK(selenium-serverコマンドを使用する時、必要！！)



### 参考URL
npmパッケージが古いため、エラーが発生。
すべてのものを最新化
http://parashuto.com/rriver/tools/updating-node-js-and-npm
### selenium-server-standalone と chromedriver をインストール

```
$ brew update
$ brew install selenium-server-standalone
$ brew install chromedriver
```

### サーバ起動

Dwebdriver.chrome.bin：使用するブラウザ指定
Dwebdriver.chrome.driver：WEBドライバー指定
```
$ /usr/local/bin/selenium-server -Dwebdriver.chrome.bin="/Applications/Google Chrome.app" -Dwebdriver.chrome.driver=/usr/local/bin/chromedriver
```

### テスト実行
```
$ mkdir selenium-chrome-test
$ cd selenium-chrome-test
$ npm install webdriverjs
$ node test-chrome.js
```
