var webdriverjs = require('webdriverjs'),
  client = webdriverjs.remote({
    desiredCapabilities: {
      browserName: 'chrome'
    },
    host: '127.0.0.1',
    port: 4444,
    logLevel: 'trace',
  }).init();

client
  .url('http://www.google.com')
  .pause(2000)
  .getTitle(function(err,title) {
    console.log(title);
  })
  .end();
