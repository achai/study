use strict;
use warnings;

open(my $in, "<", "../data/testdata") or die("could not open files");
open(my $out, ">", "../data/outputfiles") or die("could not write files");

while (<$in>) {
	print $out $_;
}

close($in);
close($out);
