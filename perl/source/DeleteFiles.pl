use strict;
use warnings;
use Fcntl;
use File::Path;


chdir "/Users/asaitakayuki/Downloads";

# get file and diretory names.
my @all_files = glob "*";
# print join("\n", @all_files) . "\n\n";

# count number
my $cnt = @all_files;

# delete files and directory
for (my $i = 0; $i < $cnt; $i++) {
   unlink $all_files[$i], "\n";
   rmtree($all_files[$i]);
}
