use strict;
use warnings;

# declare variable
my $x = 10;
my $y = 10.7;
my $z = 225_224_224;

my $xx = 10 * 10;
my $yy = 10 % 3;
my $zz = 2 ** 3;

# display messages
print $x."\n";
print $y."\n";
print $z."\n";

print $xx."\n";
print $yy."\n";
print $zz."\n";
