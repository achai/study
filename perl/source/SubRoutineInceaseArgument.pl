use strict;
use warnings;

sub max {
	my $max = shift(@_);
	foreach (@_) {
		$max = $_ if $_ > $max;
	}	
	return $max;
}

print max(10,100,200,0,10). "\n";
