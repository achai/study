## user information
- asai takayuki
  - achaikunn@gmail.com
  - asai

- suszuki ichiro
  - ichiro@gmail.com
  - ichiro
# 1. はじめに

# 2. アプリケーションの新規作成
## 2.1 初期設定
create application name by microposts.
B option is not to execute bundle install comand.
```
rails new microposts -B
```

Next, initilaze git.

```
git init
git add .
git commit -m "hogehoge..comment"
```

push created application to github.
I register remote repository and push application to github.
```
git remote add origin https://github.com/asaitakayuki/microposts.git
git push origin master
```

## 2.2 configure generator


config/application.rb



# 3.setup layouts
## 3.1. add Bootstrap

- add
  - bootstrap-pass
- delete(comment out)
  - turbolinks

### what is bootstrap-saas?(service as a application)
express function in github
https://github.com/twbs/bootstrap-sass


## 3.2.add custom scss


## 3.3 configure javascripts

app/asset/javascripts/application.js
```
//= require jquery # call jquery ?
//= require jquery_ujs # call jquery_ujs
//= require_tree . # ??
```
change configuration below(see below!!)
add bootstra-sprockets
```
//= require jquery
//= require jquery_ujs
//= require bootstrap-sprockets
```

last delete turoblinks...

## 3.4.html layouts

mircopost's all layouts.
see below detils.

app/views/layouts/application.html.erb
```
<!DOCTYPE html>
<html>
  <head>
    <title>Microposts</title>
    <%= stylesheet_link_tag 'application', media: 'all' %> #
    <%= javascript_include_tag 'application' %> # call javascirpts
    <%= csrf_meta_tags %> # ??
  </head>
  <body>
    <%= render 'layouts/header' %> # call part of templetes
    <div class="container">
      <%= yield %> # display result
    </div>
  </body>
</html>
```


app/views/layouts/_header.html.erb
```
<header class="navbar navbar-fixed-top navbar-inverse"> # include css
  <div class="container">
    <%= link_to "Microposts", '#', id: "logo" %> # top page linK??
    <nav>
      <ul class="nav navbar-nav navbar-right"> # create link to access Home,Help,Login pages
        <li><%= link_to "Home",   '#' %></li>
        <li><%= link_to "Help",   '#' %></li>
        <li><%= link_to "Log in", '#' %></li>
      </ul>
    </nav>
  </div>
</header>
```

## link_to method
### overview
create link like web site,header top

###
link_to( display_text, url)

### reference url
http://railsdoc.com/references/link_to

# 4.toppage
## 4.1.create controller and root

create controoler and mehod
```
rails gerate controller stattic_page home
```
# 5. user model
## 5.1. create model

create user model by model generater??
```
rails generate User name email passwrod-diest
```


#### what is rails generate??
refer to ruby
rails generate command use templetes to create a whole of things
（railsはコマンドを使用してテンプルを生成し、全体を作成します）


## 5.2 execute migration

```
class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.string :password_digest

      t.timestamps null: false

      t.index :email, unique: true # この行を追加
    end
  end
end
```
# 6.register function(新規登録機能)
6.1.create controller(コントローラーの作成)

```
rails generate controller user new
rails g controller user new
```

```
class UsersController < ApplicationController
  def new
    @user = User.new
  end
end

6.2. create route(ルートの作成)
resource??what do you moean and howotuse??

current execute command result(rake routes)
```
Prefix Verb URI Pattern Controller#Action
  root GET  /           static_pages#home
```

and confirm config settings.(config/route.rb)
```
Rails.application.routes.draw do
  root to: 'static_pages#home'
end
```

6.3.create form（フォーム作成）
here create top page html..(display text box.name,email,password)
these are moethod?? f is objective??
- text_filed:
- password_filed;hidden input text

oh! i understand text_filed generate html tag!!
```
<%= text_field :page, :name %>
# <input id="page_name" name="page[name]" size="30" type="text" />
```


## what is bcript?

secure algorithm
description
```
bcrypt() is a sophisticated and secure hash algorithm designed by The OpenBSD project for hashing passwords. The bcrypt Ruby gem provides a simple wrapper for safely handling passwords.

```

## 6.5. implement controllers(6.5 コントローラの実装)

```
class UsersController < ApplicationController
  # 中略

  def create # method
    @user = User.new(user_params) #
    if @user.save #whether ture of false
    else
      render 'new' # call form helper
    end
  end

  private #

  def user_params
    params.require(:user).permit(:name, :email, :password,
                                 :password_confirmation)
  end
end
```

# post function(投稿機能)
## 8.1. create model

reference is foreign key to associate User model(users tables).
refer to rails guide.
create microposts model
```
rails generate model Microposts user:reference context:text
```

## 8.3.association with user and post

## explation about association
refer to below link.
https://railsguides.jp/association_basics.html


## belogt to
belong_to asoosiate with different model(like Users and Micropost).
So,Microposts can use Users model.




8.3 コントローラとルートの作成

```
rails genearete controller Microposts
```


app/controllers/microposts_controller.rb
```

```


##8.6 destory

Let's delete messages,when mistaken tweet.

app/view/microposts/_micropost.html.erb
```
<% if current_user == micropost.user %>
      <%= link_to "delete", micropost, method: :delete,
                                       data: { confirm: "You sure?" } %>
    <% end %>
```
I check whether current_user match tweet user?
other description
(current_user(loggin_user) match with tweet_user?)

it's needs to implement delete method.


And add destory method to controller.  
app/controllers/microposts_controller.rb
```
def destory
  @micropost = current_user.microposts.find_by(id: pramams[:id])
  return redirect_to root_url if @micropost.nil?
  @micropost.destory
  flash[:success] = "Micropost deleted"
  redirect_to request.referrer || root_url
end
```


## 9.1. create model
hashimoto told that

follwer:する人
followed:される人
※I have question..

```
rails generate model Relationships follower:references followed:references

[WARNING] The model name 'Relationships' was recognized as a plural, using the singular 'Relationship' instead. Override with --force-plural or setup custom inflection rules for this noun before running the generator.
      invoke  active_record
      create    db/migrate/20170330232544_create_relationships.rb
      create    app/models/relationship.rb
      invoke    test_unit
      create      test/models/relationship_test.rb
      create      test/fixtures/relationships.yml
```

class_name must be described.rails try to search for relationship model.But occur error.()

app/model/relationship.rb
```
class Relationship < ActiveRecord::Base
  belogs_to :follower, class_name: "User"
  belogs_to :followed, class_name: "User"
end
```
why class_name descripte??(tehcaer told the reaton)

## belong_to
refer to obeject(as well as database)
http://railsdoc.com/references/belongs_to
それぞれの投稿は特定の1人のユーザーのものであり、それを紐付けるもので

db/migrate/2015XXXXXXXXXX_create_relationships.ruby-rails
```
class CreateRelationships < ActiveRecord::Migration
  def change
    create_table :relationships do |t|
      t.references :follower, index: true
      t.references :followed, index: true

      t.timestamps null: false

      t.index [:follower_id, :followed_id], unique: true # この行を追加! followerとfollowedにインデックスを貼る。ユニークを有効？あと、follower_id,followed_idにインデックスを貼っている。かつユニーク！！！
    end
  end
end
```
execute magration command.

```
rake db:migration

== 20170330232544 CreateRelationships: migrating ==============================
-- create_table(:relationships)
   -> 0.0035s
== 20170330232544 CreateRelationships: migrated (0.0036s) =====================
```


## 9.2 フォローしている人・フォローされている人の実装
ここの章は説明を主にしている所。
「following_relationships」must be same name.
foreign_key:does not means foreign key.it indicate id in user table.

app/model/user.rb
```
class User < ActiveRecord::Base

# following
  has_many : following_relationships, class_name: "RelationShip", foreign_key: "follower_id",depent: :destory
  has_many : following_users, through: :following_relationshipts source: :followed

# followed
  has_many : follower_relationships, class_name: "RelationShip", foreign_key: "followed_id", depent: destory
  has_many : follower_users, through: following_relationships, source: :follwr
end
```


## 9.3 implement unflower and flower method.

!! caaution !!
arggument(other_user) is just arggument name!!
instance variable(@user)

```
# follow ohter user
def follow(other_user)
  following_relationship.find_or_create_by(followed_id: other_user.id)
end

#  unfollower nser
def unfollow(other_user)
  following_relationship = find_or_create_by(followed_id: other_user.id)
  following_relationship.destory if following_relationship
end

# check follower status
def following?(other_user)
  following_users.include?(other_user)
end

```

## 9.4. create controller

create controller  which unfolloer and follower.
```
rails generate model Relationships
```

use method created follow nad unfollow!!
app/controllers/relationships_controller.rb
```
class RelationshipsController < ApplicationController

  def create
    @user = User.find(params[:followed_id])
    current_user.follow(@user)
  end

  def destory
    @user = current_user.following_users.find_by(params[:id]).followed
    current_user.unfollow(@user)
  end

end
```

config/routes.rb
```

```

##
login user1(asai takayuki),aceecess /user/2(kirameki..).
check follow function.but,occour error...
see below server log...

```
Started POST "/å" for 106.181.128.243 at 2017-04-03 22:20:13 +0000
Cannot render console from 106.181.128.243! Allowed networks: 127.0.0.1, ::1, 127.0.0.0/127.255.255.255

ActionController::RoutingError (uninitialized constant RelationshipsController):
  activesupport (4.2.8) lib/active_support/inflector/methods.rb:261:in `const_get'
  activesupport (4.2.8) lib/active_support/inflector/methods.rb:261:in `block in constantize'
  activesupport (4.2.8) lib/active_support/inflector/methods.rb:259:in `each'
  activesupport (4.2.8) lib/active_support/inflector/methods.rb:259:in `inject'
  activesupport (4.2.8) lib/active_support/inflector/methods.rb:259:in `constantize'
  actionpack (4.2.8) lib/action_dispatch/routing/route_set.rb:70:in `controller_reference'
  actionpack (4.2.8) lib/action_dispatch/routing/route_set.rb:60:in `controller'
  actionpack (4.2.8) lib/action_dispatch/routing/route_set.rb:39:in `serve'
  actionpack (4.2.8) lib/action_dispatch/journey/router.rb:43:in `block in serve'
  actionpack (4.2.8) lib/action_dispatch/journey/router.rb:30:in `each'
  actionpack (4.2.8) lib/action_dispatch/journey/router.rb:30:in `serve'
  actionpack (4.2.8) lib/action_dispatch/routing/route_set.rb:817:in `call'
  rack (1.6.5) lib/rack/etag.rb:24:in `call'
  rack (1.6.5) lib/rack/conditionalget.rb:38:in `call'
  rack (1.6.5) lib/rack/head.rb:13:in `call'
  actionpack (4.2.8) lib/action_dispatch/middleware/params_parser.rb:27:in `call'
  actionpack (4.2.8) lib/action_dispatch/middleware/flash.rb:260:in `call'
  rack (1.6.5) lib/rack/session/abstract/id.rb:225:in `context'
  rack (1.6.5) lib/rack/session/abstract/id.rb:220:in `call'
  actionpack (4.2.8) lib/action_dispatch/middleware/cookies.rb:560:in `call'
  activerecord (4.2.8) lib/active_record/query_cache.rb:36:in `call'
  activerecord (4.2.8) lib/active_record/connection_adapters/abstract/connection_pool.rb:653:in `call'
  activerecord (4.2.8) lib/active_record/migration.rb:377:in `call'
  actionpack (4.2.8) lib/action_dispatch/middleware/callbacks.rb:29:in `block in call'
  activesupport (4.2.8) lib/active_support/callbacks.rb:88:in `__run_callbacks__'
  activesupport (4.2.8) lib/active_support/callbacks.rb:778:in `_run_call_callbacks'
  activesupport (4.2.8) lib/active_support/callbacks.rb:81:in `run_callbacks'
  actionpack (4.2.8) lib/action_dispatch/middleware/callbacks.rb:27:in `call'
  actionpack (4.2.8) lib/action_dispatch/middleware/reloader.rb:73:in `call'
  actionpack (4.2.8) lib/action_dispatch/middleware/remote_ip.rb:78:in `call'
  actionpack (4.2.8) lib/action_dispatch/middleware/debug_exceptions.rb:17:in `call'
  web-console (2.3.0) lib/web_console/middleware.rb:20:in `block in call'
  web-console (2.3.0) lib/web_console/middleware.rb:18:in `catch'
  web-console (2.3.0) lib/web_console/middleware.rb:18:in `call'
  actionpack (4.2.8) lib/action_dispatch/middleware/show_exceptions.rb:30:in `call'
  railties (4.2.8) lib/rails/rack/logger.rb:38:in `call_app'
  railties (4.2.8) lib/rails/rack/logger.rb:20:in `block in call'
  activesupport (4.2.8) lib/active_support/tagged_logging.rb:68:in `block in tagged'
  activesupport (4.2.8) lib/active_support/tagged_logging.rb:26:in `tagged'
  activesupport (4.2.8) lib/active_support/tagged_logging.rb:68:in `tagged'
  railties (4.2.8) lib/rails/rack/logger.rb:20:in `call'
  actionpack (4.2.8) lib/action_dispatch/middleware/request_id.rb:21:in `call'
  rack (1.6.5) lib/rack/methodoverride.rb:22:in `call'
  rack (1.6.5) lib/rack/runtime.rb:18:in `call'
  activesupport (4.2.8) lib/active_support/cache/strategy/local_cache_middleware.rb:28:in `call'
  rack (1.6.5) lib/rack/lock.rb:17:in `call'
  actionpack (4.2.8) lib/action_dispatch/middleware/static.rb:120:in `call'
  rack (1.6.5) lib/rack/sendfile.rb:113:in `call'
  railties (4.2.8) lib/rails/engine.rb:518:in `call'
  railties (4.2.8) lib/rails/application.rb:165:in `call'
  rack (1.6.5) lib/rack/lock.rb:17:in `call'
  rack (1.6.5) lib/rack/content_length.rb:15:in `call'
  rack (1.6.5) lib/rack/handler/webrick.rb:88:in `service'
  /usr/local/rvm/rubies/ruby-2.3.0/lib/ruby/2.3.0/webrick/httpserver.rb:140:in `service'
  /usr/local/rvm/rubies/ruby-2.3.0/lib/ruby/2.3.0/webrick/httpserver.rb:96:in `run'
  /usr/local/rvm/rubies/ruby-2.3.0/lib/ruby/2.3.0/webrick/server.rb:296:in `block in start_thread'


  Rendered /usr/local/rvm/gems/ruby-2.3.0/gems/actionpack-4.2.8/lib/action_dispatch/middleware/templates/rescues/_trace.text.erb (0.4ms)
  Rendered /usr/local/rvm/gems/ruby-2.3.0/gems/actionpack-4.2.8/lib/action_dispatch/middleware/templates/rescues/routing_error.text.erb (5.9ms)
  ```

やっと課題が終わったー！！ゲロをはく思いでやった。
（指摘はあると思うけど、ゲロと節々が良く痛い？）

# add method to get time_line

- app/model/user.rb
 follower and myself

 # why use Microposts obeject??
 oh!
```
def feed_items
  Micropost.where(user_id : following_user_id + [self.id] )
end
```

## modify controller

- app/controller/static_page_controller.rb

```
class StaticPageControoler < ApplicationController
 def home
  if logged_in? # check login status
     @micropost = current_user.microposts.build #
     @feed_items = current_user.feed_items.inculdes(:user).order(created_at: :desc)
 end
end
```

- app/controllers/microposts_controller.rb

prepair templete to use in case of failor.

- app/controllers/microposts_controller.rb

```
def create
  @micropost = current_user.micropots.build(micropost_params)
  if @micropost.save
    flash[:success] = "Micropost Created!!"
    redirect_to root_url
  else
    @feed_items = current_user.feed_items.includes(:use).order(created_at: :desc)
    render `static_pages/home`
  end
end

```

## 10.3 create views
create partial for timeline display

- app/views/shared/_feed.html.erb
if any data exist in feed_items,display feed_items
```
<% if @feed_items.any? %>
  <ol class="microposts">
    <%= render @feed_item %>
  </ol>
<% end %>
```

add home display for timline display.

-

```
<% if logged_in? %>
  <div class="row">
    <aside class="col-md-4">
      <section class="user_info">
        <%= render 'shared/user_info' %>
      </section>
      <section class="micropost_form">
        <%= render 'shared/micropost_form' %>
      </section>
    </aside>
    <div class="col-md-8">
      <h3>Micropost Feed</h3>
      <%= render 'shared/feed' %>
    </div>
   </div>
<% else %>
  <div class="center jumbotron">
    <h1>Welcome to the Microposts</h1>

    <%= link_to "Sign up now!", signup_path, class: "btn btn-lg btn-primary" %>
  </div>
<% end %>
```
