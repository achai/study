# overview

```
ここまで出来たら動作確認を行ってください。動作確認ができたら新しいブランチを作成します。名前は、microposts-herokuとしてください。

ここで、micropostsに、オリジナルのmicropostsにない機能を追加してカスタマイズしてください。カスタマイズの内容はデザインだけなく、Rubyのコードが必要な、独自性のある機能追加が好ましいです。（この後のチャプターの「拡張してみよう」を参考にしてください。）

カスタマイズしたアプリケーションをHerokuにアップロードして動かしてください。動かし方はメッセージボードのレッスンなどを参考にします。

完成したらGitHubにブランチをプッシュして、リポジトリ名(microposts)とブランチ名(microposts-heroku)を選択してコメントに必ずアプリケーションのHerokuのURLと実装したオリジナル機能の内容を記載して提出をおこなってください。

 必ず、アプリケーションのHerokuのURLにアクセスして、オリジナル機能が動作することを確認してください。
※ 課題提出後はmasterブランチに戻って作業を行ってください。
※ コードレビューで合格になった後に、microposts-herokuブランチをmasterブランチにマージしてください。

提出時の注意

提出フォームの項目にある「動作確認用URL」には「HerokuのURL」を記載してください。
```


# information

heroku:https://asai-retweet-2.herokuapp.com/

# todo

- create branch(micropost-heroku)
- create retweet function
- deploy heroku

# create new branch(micropost-heroku)

add all file to working(stageing)
```
git add .
```

commit to local repository
```
git commmit -m "{comment}"
```

Then,create new branch.
```
git checkout -b micropost-heroku
```



http://

# review

# 1


## 2

課題の再提出ありがとうございます。

大変申し訳無いのですが、githubからcloneしたコードと herokuにデプロイされているコードが異なっているようです。

githubからcloneしたコードでは retweetした場合に、retweetであることを示す情報は表示されていませんでした。

https://gyazo.com/f6bb6eda9e08f805fc04fbbbd980ecf7

herokuでのコードではリツイートについて、元になったツイート情報として IDを表示していただいているのですが、

https://gyazo.com/9d43c5b5fac6089d51d89a4e8a20936f

ここはその元になったツイートを投稿したユーザー名などの表示をお願い致します。（可能であれば、そのユーザーへのリンクがあると更に良くなると思います。）

また、上記画像の2番めのツイートをretweetの箇所をクリックすると

https://gyazo.com/153d38977f844a6417ff3c2fa74befad

のようなエラーが発生いたしました。

github上のコードとherokuのデプロイコードが異なっているので、こちらでの原因箇所の特定は難しいのですが、見直してみていただけますでしょうか？

上記、ご検討の上、修正して、再提出をお願いいたします。
