# overview

## what is favrite function?
gether favrite teweet(own,other)
https://syncer.jp/twitter-how-to-use-favorite

##

router
model
controller
view
each tweet has star button.

# reference url
code from github.
https://github.com/daraf-jp/tsubuyaki_22_nov/search?utf8=%E2%9C%93&q=favorite&type=

how to make and flow...
https://cyllabus.jp/courses/LVlXWNb8GcWTKy8x1gHS5g
# work


## create model
many to many(user to tweet).

```
achai3:~/workspace/microposts (favorite) $ rails g model favorite user_id:integer, micropost_id:integer
      invoke  active_record
      create    db/migrate/20170422073004_create_favorites.rb
      create    app/models/favorite.rb
      invoke    test_unit
      create      test/models/favorite_test.rb
      create      test/fixtures/favorites.yml

```

## index
to improve serach speed(user_id,micropost_id)

db/migrate/20170422073004_create_favorites.rb
```
t.index :user_id
t.index :micropost_id
```

## execute migration
generate table,index....
caution!! before execute migration,there are no tables and schemas.
git do not manage database code!!

```
== 20170422073004 CreateFavorites: migrating ==================================
-- create_table(:favorites)
   -> 0.0035s
== 20170422073004 CreateFavorites: migrated (0.0036s) =========================

```

## routeing

bofore add routering,check router...

```
rake routes
           Prefix Verb   URI Pattern                       Controller#Action
             root GET    /                                 static_pages#home
           signup GET    /signup(.:format)                 users#new
            login GET    /login(.:format)                  sessions#new
                  POST   /login(.:format)                  sessions#create
           logout DELETE /logout(.:format)                 sessions#destory
   following_user GET    /users/:id/following(.:format)    users#following
    follower_user GET    /users/:id/follower(.:format)     users#follower
            users GET    /users(.:format)                  users#index
                  POST   /users(.:format)                  users#create
         new_user GET    /users/new(.:format)              users#new
        edit_user GET    /users/:id/edit(.:format)         users#edit
             user GET    /users/:id(.:format)              users#show
                  PATCH  /users/:id(.:format)              users#update
                  PUT    /users/:id(.:format)              users#update
                  DELETE /users/:id(.:format)              users#destroy
retweet_micropost GET    /microposts/:id/retweet(.:format) microposts#retweet
       microposts GET    /microposts(.:format)             microposts#index
                  POST   /microposts(.:format)             microposts#create
    new_micropost GET    /microposts/new(.:format)         microposts#new
   edit_micropost GET    /microposts/:id/edit(.:format)    microposts#edit
        micropost GET    /microposts/:id(.:format)         microposts#show
                  PATCH  /microposts/:id(.:format)         microposts#update
                  PUT    /microposts/:id(.:format)         microposts#update
                  DELETE /microposts/:id(.:format)         microposts#destroy
    relationships POST   /relationships(.:format)          relationships#create
     relationship DELETE /relationships/:id(.:format)      relationships#destroyrake router
```

add micropost and user routes.
look at belowing code.

config/routes.rb
```
resouces :microposts do
  resoueces :favorites, only: [:create, :destroy]
  end

resoueces :users do
  get :favorites, on: :member
  end
```

after routes settings,execute rake db....

```
achai3:~/workspace/microposts (favorite) $ rake routes
             Prefix Verb   URI Pattern                                       Controller#Action
               root GET    /                                                 static_pages#home
             signup GET    /signup(.:format)                                 users#new
              login GET    /login(.:format)                                  sessions#new
                    POST   /login(.:format)                                  sessions#create
             logout DELETE /logout(.:format)                                 sessions#destory
     favorites_user GET    /users/:id/favorites(.:format)                    users#favorites
     following_user GET    /users/:id/following(.:format)                    users#following
      follower_user GET    /users/:id/follower(.:format)                     users#follower
              users GET    /users(.:format)                                  users#index
                    POST   /users(.:format)                                  users#create
           new_user GET    /users/new(.:format)                              users#new
          edit_user GET    /users/:id/edit(.:format)                         users#edit
               user GET    /users/:id(.:format)                              users#show
                    PATCH  /users/:id(.:format)                              users#update
                    PUT    /users/:id(.:format)                              users#update
                    DELETE /users/:id(.:format)                              users#destroy
micropost_favorites POST   /microposts/:micropost_id/favorites(.:format)     favorites#create
 micropost_favorite DELETE /microposts/:micropost_id/favorites/:id(.:format) favorites#destroy
  retweet_micropost GET    /microposts/:id/retweet(.:format)                 microposts#retweet
         microposts GET    /microposts(.:format)                             microposts#index
                    POST   /microposts(.:format)                             microposts#create
      new_micropost GET    /microposts/new(.:format)                         microposts#new
     edit_micropost GET    /microposts/:id/edit(.:format)                    microposts#edit
          micropost GET    /microposts/:id(.:format)                         microposts#show
                    PATCH  /microposts/:id(.:format)                         microposts#update
                    PUT    /microposts/:id(.:format)                         microposts#update
                    DELETE /microposts/:id(.:format)                         microposts#destroy
      relationships POST   /relationships(.:format)                          relationships#create
       relationship DELETE /relationships/:id(.:format)                      relationships#destroy
```


[what_is_member](../document/what_is_member.md)

## create controller

app/controllers/favorites_controller.rb
```
class FavoritesController < ApplicationController

before_action :logged_in_user, only: [:favrite]

  def create
    @tweet = Tweet.find(params[:id])
    @favorite = current_user.favorites.bulid(tweet: @tweet)
    if @favorite.save
      redirect_to tweets_url, notice: "お気に入りを追加しました"
    else
      redirect_to tweets_url, alert: "お気に入りを追加できません"
    end

  def destroy
    @favorite = current_user.favorites.find_by(params[:id])
    @favorite.destroy
    redirect_to tweets_url, notice: "お気に入りを解除しました"
  end
end
```

User#show

app/controllers/users_cntroller.rb
```
def favorites
  @user = User.find(params[:id])
end
```


# create model

- User
- Micropost
- favorite


app/models/favorite.rb

favorite⇒user
favorite⇒micropost
```
belongs_to :user
belongs_to :micropot
```

app/models/micropost.rb
dependent user destroy method..
```
has_many :favorites, dependent: :destroy
```

app/model/user.rb
```
has_many :favorites, dependent: :destory
```

```

```

# option :depend

depend option(destroy)

```
@microposts = Micropost.where(id: @favroites.id)
@microposts.each do |micropost|
  micropost.destroy
end
@favorites.destroy
```

# define validation

```
# user type is not null.
validates :user, presence: true

# user_id(*)
validates :user_id, uniqueness: { scope: :tweet_id }

# micropost is not null.
validates :micropost, presence: true
```

## uniqueness
this attribute's value is unique right before the object gets saved.
it may happens that two deffirenet databse creates two records in the same value.
To avoid that

(たか翻訳：)
# check subscribe

app/model/tweet.rb

```
def favorited_by? user
  favorites.where(user_id: user.id).exist?
end
```

# favorite link

```
= link_to "お気に入りに登録", tweet_favorites_path(t), method: :post
53	                 - if t.favorited_by? current_user
54	                   = link_to "お気に入りの解除", tweet_favorites_path(t), method: :delete
55	                 - else
56	                   = link_to "お気に入りに登録", tweet_favorites_path(t), method: :post
```
