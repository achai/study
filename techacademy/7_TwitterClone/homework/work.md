

```
課題：HTMLのアレンジ

メッセージボードアプリの投稿内容の入力部分をinputタグではなくtextareaタグにしてください。
→済

また、それに伴いHTMLをBootstrapを使ってform要素のデザインを整えてください。フォームは基本例で紹介されているやり方で実装してください。

GitHubのmessage-boardにブランチ名「challenge1」を作成して提出して下さい。課題提出用（challenge1ブランチ）と教材進捗用（masterブランチ）と分けることで、2つを独立して進めるようにします。課題に合格した後は、課題提出用（challenge1ブランチ）を教材進捗用（masterブランチ）へマージ（統合）します。

注意点

課題提出後はmasterブランチに戻って作業を行ってください。
コードレビューで合格になった後に、challenge1ブランチをmasterブランチにマージしてください。
```
## howto
###
```
また、それに伴いHTMLをBootstrapを使ってform要素のデザインを整えてください。フォームは基本例で紹介されているやり方で実装してください。
```
viewの所にサンプロコードをそのままコピペする。それをどうやって、Bootstrapに実装させるか、考えて、viewを編集し、デプロイし、

## change tag(input textarea)
http://qiita.com/labocho/items/a7afb80269b5c7556fa2
!!問題文をちゃんと読むこと！！入力部分のみ、inputタグではなくtextareaタグで実装せよ！！

```
<form class="new_message" id="new_message" action="/messages" accept-charset="UTF-8" method="post"><input name="utf8" type="hidden" value="&#x2713;" /><input type="hidden" name="authenticity_token" value="zZ7pQ3/25BTv8KhQQBaXiopMM56Nx7U1nPIZSHL354rSOIWuDz8bqx8hXOdRqtiH7LCphmrlYBEPeXgkd9lgQQ==" />
  名前:
  <textarea name="message[name]" id="message_name">
</textarea>

  内容:
  <textarea name="message[body]" id="message_body">
</textarea>
  <input type="submit" name="commit" value="Create Message" class="btn btn-sm btn-primary" />
</form>
```
## add bootstrap


## form_for
これ自体は、フォーム（お問い合わせ等を作る物）
次に中身のf(オブジェクトなのか？)と
```
<%= f.text_field :name, class: 'form-control', id: 'name' %>
```
ということは、これからこんな書式を書ける

```
<% %>：これ自体は、ソースをhtmlタグ内に書き出すよというやつ。テキストにのっていた。
```


##  fix item

## form_for
- add content shadow(name,body) done
- change text area(body)  skip
- content right?? text_area's specification.avaliable resize: none; but it's fixed filed.


# question and answer


```
lesson6の課題：HTMLのアレンジについて質問があります。
内容には、課題の指定があったように「textarea」タグを使用しています。
が、右下のちょんちょん（なんて表現したらいいかわかりませんが）は、仕様なのでしょうか？
https://gyazo.com/19d8c8661cf4cfaa2f3cc5380cb0ab4a
「text_field」と同様にしたいです。消すことは可能でしょうか？

右下の表示はtextareaの仕様です。
こちらをドラッグすると入力スペースを拡大/縮小できます。

[5:25]  
リサイズ不可にするとこの表示を消すことができますね。

[5:26]  
resize: none; を指定してください。

takayuki.asai [5:28 PM]
そんなことができるですね！
拡大、縮小できる方がユーザにはやさしいですね。
そのままにします。。
ありがとうございます！
```
