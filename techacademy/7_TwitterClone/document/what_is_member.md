# what is member??
I refer to rails guide.
look at folloing description..

```
Within the block of member routes, each route name specifies the HTTP verb will be recognized.
You can use get, patch, put, post, or delete here .
If you don't have multiple member routes, you can also pass :on to a route, eliminating the block:
（メンバー内では、それぞれのルートネームがを認識する。使えるものは、get,patch,put,post,delete...である。複数のメンバールートを持ってないと、次の方法でも使える）

resources :photos do
  get 'preview', on: :member
end

same meanings.
firstball useage in behind count.

resources :photos do
  member do
   get 'preview'
  end
end

uri_pattern:photos/1/preview
http_request:get

route to preview actions
```
