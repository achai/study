# overview
howto use 「rails c(console)」 command.

## check database column's name...
each of object(User,Micropost,Reationship) would like to confirm column's name, data type..and so on...

```
User.columns
 => [#<ActiveRecord::ConnectionAdapters::Column:0x00000005707da8 @name="id", @cast_type=#<ActiveRecord::Type::Integer:0x0000000575ca10 @precision=nil, @scale=nil, @limit=nil, @range=-2147483648...2147483648>, @sql_type="INTEGER", @null=false, @default=nil, @default_function=nil>, #<ActiveRecord::ConnectionAdapters::Column:0x00000005707ba0 @name="name", @cast_type=#<ActiveRecord::Type::String:0x00000005719760 @precision=nil, @scale=nil, @limit=nil>, @sql_type="varchar", @null=true, @default=nil, @default_function=nil>, #<ActiveRecord::ConnectionAdapters::Column:0x00000005707a88 @name="email", @cast_type=#<ActiveRecord::Type::String:0x00000005719760 @precision=nil, @scale=nil, @limit=nil>, @sql_type="varchar", @null=true, @default=nil, @default_function=nil>, #<ActiveRecord::ConnectionAdapters::Column:0x00000005707920 @name="password_digest", @cast_type=#<ActiveRecord::Type::String:0x00000005719760 @precision=nil, @scale=nil, @limit=nil>, @sql_type="varchar", @null=true, @default=nil, @default_function=nil>, #<ActiveRecord::ConnectionAdapters::Column:0x00000005707740 @name="created_at", @cast_type=#<ActiveRecord::Type::DateTime:0x00000005718e00 @precision=nil, @scale=nil, @limit=nil>, @sql_type="datetime", @null=false, @default=nil, @default_function=nil>, #<ActiveRecord::ConnectionAdapters::Column:0x00000005707560 @name="updated_at", @cast_type=#<ActiveRecord::Type::DateTime:0x00000005718e00 @precision=nil, @scale=nil, @limit=nil>, @sql_type="datetime", @null=false, @default=nil, @default_function=nil>, #<ActiveRecord::ConnectionAdapters::Column:0x00000005707420 @name="country", @cast_type=#<ActiveRecord::Type::Value:0x00000005718450 @precision=nil, @scale=nil, @limit=nil>, @sql_type="stirng", @null=true, @default=nil, @default_function=nil>]
```

uh?? I don't know meaings...

```
User.columns.map(&:name)
 => ["id", "name", "email", "password_digest", "created_at", "updated_at", "country"]
Micropost.columns.map(&:name)
```
OK! I got it!


## check database

each of object's model get select data.

```
# generate database
rails db

# execute sql query

select * from microposts;
["id", "user_id", "content", "created_at", "updated_at", "micropost_id"]
1|1|aaaa|2017-04-10 05:51:18.882348|2017-04-10 05:51:18.882348|
2|1|bbbb|2017-04-10 05:51:25.522739|2017-04-11 11:49:49.174504|1
3|2|ichiro dayo!|2017-04-10 05:53:39.720390|2017-04-10 05:53:39.720390|
4|2|baseball player!!|2017-04-10 05:53:50.727520|2017-04-10 05:53:50.727520|
```



### reference URL
http://qiita.com/littlekbt/items/48fa2b428147921db5a5


> _MICROPOST.html.erb
>
> <%= link_to (image_tag "retweet.png"), reaspas_micropost_path(micropost) %>
