class Shop
 def initialize(name)
  @name = name
 end

# get values called by seter(reader) 
 def name
  @name
 end

# set values into variable called by seter(writer) 
 def name=(value)
  @name = value
 end
end
