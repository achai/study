
Lesson7 7.5.ヘルパーメソッドの追加のコードついて、質問があります。

```
def current_user
   @current_user ||= User.find_by(id: session[:user_id])
 end
```

最初の段階では、current_userインスタンスには、初めは何も入っていない(nil)でよいですか？
ページ遷移するたびにcurrent_userメソッドを実行し、ログイン状態か確認を行っていますか？？


## 2
9.1 モデルの作成について、質問があります。
> フォローしているユーザーをfollower、フォローされているユーザーをfollowedとして
twitterを考えると、「follower」は、自分をフォローしている人、「followed」は、自分がフォローしている人という認識でよいですか？
ß
