# 質問

イグレーションを行うとエラーが発生します。

[3:12]
 ```achai3:~/workspace/microposts (master) $ rake db:migrate:reset
== 20170323215910 CreateUsers: migrating ======================================
-- create_table(:users)
   -> 0.0020s
== 20170323215910 CreateUsers: migrated (0.0020s) =============================

== 20170328230754 AddCountryToUsers: migrating ================================
-- add_column(:users, :country, :string)
   -> 0.0005s
== 20170328230754 AddCountryToUsers: migrated (0.0006s) =======================

== 20170330060838 CreateMicroposts: migrating =================================
-- create_table(:microposts)
   -> 0.0023s
== 20170330060838 CreateMicroposts: migrated (0.0024s) ========================

== 20170402230511 CreateRelationships: migrating ==============================
-- create_table(:relationships)
   -> 0.0030s
== 20170402230511 CreateRelationships: migrated (0.0031s) =====================

== 20170412000017 AddMicropostToMicropost: migrating ==========================
-- add_column(:microposts, :micropost_id, {:index=>true, :foreign_key=>true})
rake aborted!
StandardError: An error has occurred, this and all later migrations canceled:

undefined method `to_sym' for {:index=>true, :foreign_key=>true}:Hash
Did you mean?  to_s
               to_yaml
               to_set
```

[3:13]
20170412000017_add_micropost_to_micropost.rb

[3:13]
 ```    add_column :microposts, :micropost_id, index: true, foreign_key: true
```

[3:14]
で問題が発生しているのです。

[3:14]
カラム項目がなぜか下記のようになってしまいます。

[3:14]
 ```2.3.0 :011 > Micropost.column_names
 => ["id", "user_id", "content", "created_at", "updated_at", "micropost_id_id"]
```
(edited)

[3:15]
カラム名を「micropost_id」としたいのですが、どうすればよいでしょうか？

`add_reference` でも良いかと思います。
試しになのですが `add_column :microposts, :micropost_id, index: true, foreign_key: true` の `foreign_key: true` を `foreign_key: false` にして実行すると、どうなるでしょうか？

うまくいきました。
foreign_keyは外部参照キーだから、指定したカラム（micropost_id）がどことキーを結べばよいかがわからないからエラーになったと考えておりますが、どうでしょうか？
別件ですが、ルビーのエラーはなぜこれほどわかりにくいのでしょうか？

エラーは確かに分かりづらいと思います。特にマイグレーション関係は直接的な表現（どこそこがおかしい）が無い場合が多く、デバッグに時間を要しますね。個人的にですが、おそらく様々なデータベース（MySQL、Postgres、SQL Server、SQLite ...）にマイグレーションを対応させるため、表面的な表現に留めているといった気がします。
