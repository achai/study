# question
I think return value is diffencet.

- where
- find

## exmpale

```
rails c

# get first record
user = User.first

User Load (0.1ms)  SELECT  "users".* FROM "users"  ORDER BY "users"."id" ASC LIMIT 1
 => #<User id: 1, name: "asaitakayuki", email: "achaikunn@gmail.com", password_digest: "$2a$10$M4aqvH6x5YdBc9PzWA2cuuNZgvbVBQvXA6ujgfnZDnq...", created_at: "2017-04-15 07:41:05", updated_at: "2017-04-18 09:38:59", country: nil>

 2.3.0 :075 > user.follower_users
   User Load (0.3ms)  SELECT "users".* FROM "users" INNER JOIN "relationships" ON "users"."id" = "relationships"."follower_id" WHERE "relationships"."followed_id" = ?  [["followed_id", 1]]
  => #<ActiveRecord::Associations::CollectionProxy [#<User id: 2, name: "smith", email: "smith@gmail.com", password_digest: "$2a$10$REgur0LWm6sgg4Z/3ksdHuzBS0oZkkjXN/fZc1.dVV2...", created_at: "2017-04-15 08:19:59", updated_at: "2017-04-15 08:19:59", country: nil>]>

user = User.where(id: 1)
User Load (0.3ms)  SELECT "users".* FROM "users" WHERE "users"."id" = ?  [["id", 1]]
=> #<ActiveRecord::Relation [#<User id: 1, name: "asaitakayuki", email: "achaikunn@gmail.com", password_digest: "$2a$10$M4aqvH6x5YdBc9PzWA2cuuNZgvbVBQvXA6ujgfnZDnq...", created_at: "2017-04-15 07:41:05", updated_at: "2017-04-18 09:38:59", country: nil>]>

user.following_users
NoMethodError: undefined method `following_users' for #<User::ActiveRecord_Relation:0x00000002a7ca68>
        from /usr/local/rvm/gems/ruby-2.3.0/gems/activerecord-4.2.8/lib/active_record/relation/delegation.rb:136:in `method_missing'
        from /usr/local/rvm/gems/ruby-2.3.0/gems/activerecord-4.2.8/lib/active_record/relation/delegation.rb:99:in `method_missing'
        from (irb):80
        from /usr/local/rvm/gems/ruby-2.3.0/gems/railties-4.2.8/lib/rails/commands/console.rb:110:in `start'
        from /usr/local/rvm/gems/ruby-2.3.0/gems/railties-4.2.8/lib/rails/commands/console.rb:9:in `start'
        from /usr/local/rvm/gems/ruby-2.3.0/gems/railties-4.2.8/lib/rails/commands/commands_tasks.rb:68:in `console'
        from /usr/local/rvm/gems/ruby-2.3.0/gems/railties-4.2.8/lib/rails/commands/commands_tasks.rb:39:in `run_command!'
        from /usr/local/rvm/gems/ruby-2.3.0/gems/railties-4.2.8/lib/rails/commands.rb:17:in `<top (required)>'
        from bin/rails:4:in `require'
        from bin/rails:4:in `<main>'

```

whereとfirstでレコードを取得しているのですが、whereはfollower_usersで取得ができません。（メソッドの定義エラーになってしまいます。）
どこに違いがあるのでしょうか？

## active association collection proxy
class..method???
http://api.rubyonrails.org/classes/ActiveRecord/Associations/CollectionProxy.html
