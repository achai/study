mentor-uchiyama [9:07 PM]
「buildメソッドで引数ありで使用する場合は、ハッシュ形式の引数にしてね」というエラーですね

mentor-uchiyama [9:18 PM]
> オリジナルのツイートをそのままコピーしようと考えております。
これを実現するためには、「誰(=current_user)」が「誰のツイート(=other_user? following_user? > micropost)」を「リツイートする(=retweet action)」と考えるといいのかなと思います。

[9:22]
現在起こっているエラーと直接関係ない事柄へのアドバイスでしたね^^;すいません。
buildメソッドを使うのは問題ないですが、引数に渡す形を `object.attributes` とかにするとどうかな、って思います。

mentor-uchiyama [9:40 PM]
`microposts_controller#retweet` 24行目、いい感じですね^^ (edited)

[9:41]
あとは、今のエラーが発生している25行目ですね。

mentor-uchiyama [10:01 PM]
参考になりそうなコードがありましたので、情報共有しますね^^
https://gist.github.com/richardsondx/2485533 (edited)


----- Today April 14th, 2017 -----
takayuki.asai [7:16 AM]
返信ありがとうございます！

takayuki.asai [8:17 AM]
うまく動きました！！
（ネットで検索する時、うまく問題に対しての対応を引き出せません。英語のサイトも見るようにしてますが） (edited)
 3 replies Last reply today at 4:09 PM View thread

takayuki.asai [8:44 AM]
課題：オリジナル機能追加とHerokuへのデプロイですが、リツイートを実装しました。

takayuki.asai [8:46 AM]
他の人のものをツイートするだけですが、機能として問題ないでしょうか？
 2 replies Last reply today at 3:29 PM View thread

takayuki.asai [6:16 PM]
herokuにデプロイする所でエラーが発生します。

[6:17]
 ```PG::UndefinedObject: ERROR:  type "stirng" does not exist
LINE 1: ALTER TABLE "users" ADD "country" stirng
                                          ^
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/connection_adapters/postgresql/database_statements.rb:155:in `async_exec'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/connection_adapters/postgresql/database_statements.rb:155:in `block in execute'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/connection_adapters/abstract_adapter.rb:484:in `block in log'
/app/vendor/bundle/ruby/2.3.0/gems/activesupport-4.2.8/lib/active_support/notifications/instrumenter.rb:20:in `instrument'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/connection_adapters/abstract_adapter.rb:478:in `log'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/connection_adapters/postgresql/database_statements.rb:154:in `execute'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/connection_adapters/abstract/schema_statements.rb:407:in `add_column'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/connection_adapters/postgresql/schema_statements.rb:434:in `add_column'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/migration.rb:665:in `block in method_missing'
```


mentor-aihara [6:19 PM]
```PG::UndefinedObject: ERROR:  type "stirng" does not exist
LINE 1: ALTER TABLE "users" ADD "country" stirng
```

この段にありますように `stirng` という type がないというエラーになります。
新しく追加したソースコード等に `stirng` とタイポされている箇所がないかどうか確認してみてください。

[6:20]
正しくは `string` でしょうか。

takayuki.asai [6:39 PM]
解決しました！
が、他の同じようなエラーが発生しています。
今までもイミグレーションを行っていましたが、エラーは発生しませんでした。

[6:39]
元々、sqliteを使っていたからでしょうか？

```PG::UndefinedObject: ERROR:  type "stirng" does not exist
LINE 1: ALTER TABLE "users" ADD "country" stirng
```

この段にありますように `stirng` という type がないというエラーになります。
新しく追加したソースコード等に `stirng` とタイポされている箇所がないかどうか確認してみてください。

[6:20]
正しくは `string` でしょうか。

takayuki.asai [6:39 PM]
解決しました！
が、他の同じようなエラーが発生しています。
今までもイミグレーションを行っていましたが、エラーは発生しませんでした。

[6:39]
元々、sqliteを使っていたからでしょうか？

mentor-aihara [6:42 PM]
「他の同じようなエラー」を、Gyazo 等で共有頂いてもよろしいでしょうか？
基本的には SQLite であっても、そうでなくても、マイグレーションには差が出ないようにはなっています。

takayuki.asai [6:42 PM



```
achai3:~/workspace/microposts (micropost-heroku) $ heroku run rake db:migrate
Running rake db:migrate on ⬢ tasai-retweet-function... up, run.5797 (Free)
== 20170412000017 AddMicropostToMicropost: migrating ==========================
-- add_column(:microposts, :micropost_id, {:index=>true, :foreign_key=>true})
rake aborted!
StandardError: An error has occurred, this and all later migrations canceled:

undefined method `to_sym' for {:index=>true, :foreign_key=>true}:Hash
Did you mean?  to_s
               to_yaml
               to_set
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/connection_adapters/abstract/schema_definitions.rb:391:in `add_column'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/connection_adapters/abstract/schema_statements.rb:406:in `add_column'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/connection_adapters/postgresql/schema_statements.rb:434:in `add_column'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/migration.rb:665:in `block in method_missing'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/migration.rb:634:in `block in say_with_time'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/migration.rb:634:in `say_with_time'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/migration.rb:654:in `method_missing'
/app/db/migrate/20170412000017_add_micropost_to_micropost.rb:3:in `change'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/migration.rb:608:in `exec_migration'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/migration.rb:592:in `block (2 levels) in migrate'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/migration.rb:591:in `block in migrate'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/connection_adapters/abstract/connection_pool.rb:292:in `with_connection'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/migration.rb:590:in `migrate'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/migration.rb:768:in `migrate'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/migration.rb:998:in `block in execute_migration_in_transaction'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/migration.rb:1044:in `block in ddl_transaction'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/connection_adapters/abstract/database_statements.rb:213:in `block in transaction'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/connection_adapters/abstract/transaction.rb:184:in `within_new_transaction'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/connection_adapters/abstract/database_statements.rb:213:in `transaction'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/transactions.rb:220:in `transaction'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/migration.rb:1044:in `ddl_transaction'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/migration.rb:997:in `execute_migration_in_transaction'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/migration.rb:959:in `block in migrate'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/migration.rb:955:in `each'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/migration.rb:955:in `migrate'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/migration.rb:823:in `up'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/migration.rb:801:in `migrate'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/tasks/database_tasks.rb:137:in `migrate'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/railties/databases.rake:44:in `block (2 levels) in <top (required)>'
/app/vendor/bundle/ruby/2.3.0/gems/rake-12.0.0/exe/rake:27:in `<top (required)>'
/app/vendor/bundle/ruby/2.3.0/gems/bundler-1.13.7/lib/bundler/cli/exec.rb:74:in `load'
/app/vendor/bundle/ruby/2.3.0/gems/bundler-1.13.7/lib/bundler/cli/exec.rb:74:in `kernel_load'
/app/vendor/bundle/ruby/2.3.0/gems/bundler-1.13.7/lib/bundler/cli/exec.rb:27:in `run'
/app/vendor/bundle/ruby/2.3.0/gems/bundler-1.13.7/lib/bundler/cli.rb:332:in `exec'
/app/vendor/bundle/ruby/2.3.0/gems/bundler-1.13.7/lib/bundler/vendor/thor/lib/thor/command.rb:27:in `run'
/app/vendor/bundle/ruby/2.3.0/gems/bundler-1.13.7/lib/bundler/vendor/thor/lib/thor/invocation.rb:126:in `invoke_command'
/app/vendor/bundle/ruby/2.3.0/gems/bundler-1.13.7/lib/bundler/vendor/thor/lib/thor.rb:359:in `dispatch'
/app/vendor/bundle/ruby/2.3.0/gems/bundler-1.13.7/lib/bundler/cli.rb:20:in `dispatch'
/app/vendor/bundle/ruby/2.3.0/gems/bundler-1.13.7/lib/bundler/vendor/thor/lib/thor/base.rb:440:in `start'
/app/vendor/bundle/ruby/2.3.0/gems/bundler-1.13.7/lib/bundler/cli.rb:11:in `start'
/app/vendor/bundle/ruby/2.3.0/gems/bundler-1.13.7/exe/bundle:34:in `block in <top (required)>'
/app/vendor/bundle/ruby/2.3.0/gems/bundler-1.13.7/lib/bundler/friendly_errors.rb:100:in `with_friendly_errors'
/app/vendor/bundle/ruby/2.3.0/gems/bundler-1.13.7/exe/bundle:26:in `<top (required)>'
/app/bin/bundle:3:in `load'
/app/bin/bundle:3:in `<main>'
NoMethodError: undefined method `to_sym' for {:index=>true, :foreign_key=>true}:Hash
Did you mean?  to_s
               to_yaml
               to_set
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/connection_adapters/abstract/schema_definitions.rb:391:in `add_column'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/connection_adapters/abstract/schema_statements.rb:406:in `add_column'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/connection_adapters/postgresql/schema_statements.rb:434:in `add_column'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/migration.rb:665:in `block in method_missing'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/migration.rb:634:in `block in say_with_time'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/migration.rb:634:in `say_with_time'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/migration.rb:654:in `method_missing'
/app/db/migrate/20170412000017_add_micropost_to_micropost.rb:3:in `change'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/migration.rb:608:in `exec_migration'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/migration.rb:592:in `block (2 levels) in migrate'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/migration.rb:591:in `block in migrate'
/app/vendor/bundle/ruby/2.3.0/gems/activerecord-4.2.8/lib/active_record/connection_adapters/abstract/connection_pool.rb:292:in `with_connection'
```

Cloud9 の該当プロジェクトを確認させて頂きました。
`20170412000017_add_micropost_to_micropost.rb` で `add_column` を実施されていますが、追加される際の型の記載がないようです。
`20170328230754_add_country_to_users.rb` を参考に、型を指定してみてください。 `id` の場合は一般的には `:integer` でしょうか。

mentor-aihara [6:59 PM]
`rails g migration` コマンドを実施された際に `〜:string` とタイプすべきところを `〜:stirng` としてしまった可能性がございます。
その場合 `rails g migration` コマンドは特にエラーを出しません（出してくれると良いんですけどね）。

タイポはそのまま `db/migrate/` ディレクトリに出力されたソースコードに反映されるという仕組みになっておりますので、同様の現象が発生した場合は
(1) `rails destroy migration` でマイグレーションファイルを削除する
か、
(2) 出力されたマイグレーションファイルを修正する
のいずれかを実施するかたちになります。
基本的にはどちらを実施しても同じ結果になりますが、カラムの指定が多いと (2) で面倒な場合もありますので、個人的には (1) をおすすめします。ご参考下さい。
