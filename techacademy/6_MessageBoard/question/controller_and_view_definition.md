


```
A controller's purpose is to receive specific requests for the application. Routing decides which controller receives which requests. Often, there is more than one route to each controller, and different routes can be served by different actions. Each action's purpose is to collect information to provide it to a view.

# 翻訳（たかちゃん）
(コントローラーの目的は、特定のリクエストをうける。ルーティングはどのコントローラーがどのリクエストをうけるか決定する。
１つのルーティングが各コントローラーよりある？？異なるルーティングが異なるアクションを供給する。それぞれ（ここではルーテ
ィングとアクションのことを指す）の目的は、viewに供給する情報を収集する。)

# 翻訳（グーグル先生）
コントローラの目的は、アプリケーションの特定の要求を受信することです。ルーティングは、どのコントローラがどの要求を受信するかを決定します。多くの場合、各コントローラに複数のルートがあり、異なるルートによって異なるルートが提供されます。各アクションの目的は、情報を収集してビューに提供することです。

A view's purpose is to display this information in a human readable format. An important distinction to make is that it is the controller, not the view, where information is collected. The view should just display that information. By default, view templates are written in a language called eRuby (Embedded Ruby) which is processed by the request cycle in Rails before being sent to the user.

# 翻訳（たかちゃん）
ビューの目的は、人が読めるフォーマットに情報を表示することです。（コントローラー、モデルが処理した内容を表示する）
重要なXXはコントローラーである、情報が集められているビューじゃないよ（※）ビューはただ、情報を表示するだけだよ。デフォルトでは、ビューテンプレートは「eRuby」という言語？で書かれているよ。

# 翻訳（google先生）
ビューの目的は、この情報を人間が読める形式で表示することです。重要な違いは、情報が収集されるビューではなくコントローラであることです。ビューはその情報を表示するだけです。デフォルトでは、ビューテンプレートはeRuby（Embedded Ruby）と呼ばれる言語で記述され、ユーザに送信される前にRailsのリクエストサイクルで処理されます。

```

※：文法的にここにかかっているだけ？？

# unkonwn words
- distinction

# 質問2
## 8.5 フラッシュメッセージ

下記ソースの処理がよく理解しておりません。
１つ１つに処理内容を記載しましたが、理解は間違っていないでしょうか？
特に、下記がよくわかりません。

- <%= notice %>は、「<%= %>」で処理結果をHTMLに埋め込むと考えております。「notice」はメッセージ内容を表示させるのでしょうか？
- <%= yield %>は、どこコントローラの処理結果をうめこんでいるのでしょうか？

```
<html>
  <!-- 中略 -->
  <div class="container">
    <% if notice %> # noticeが正（何か表示するものがある場合）
      <div class="alert alert-info"> #ここはただのCSSを読み込んでいる
        <%= notice %> # noticeの内容を表示しているのか？
      </div>
    <% end %> # if条件式の範囲

    <%= yield %> # コントローラーの処理結果を埋め込む。どの処理を埋め込んでいるのか？
  </div>
</body>
</html>
```
## 回答２
noticeは予約語で特別な変数です。
フラッシュとして使います。

[4:47]  
https://gyazo.com/615ef2310cdd9ad83e54539d8f0bd7a1
Gyazo (45KB)

[4:48]  
ここで値を渡しているからです。

[4:49]  
こちらも参考にしていただくと良いと思います。
http://ruby-rails.hatenadiary.com/entry/20141127/1417086075
Rails Webook
Rails4でユーザーに簡易なメッセージをflashで表示する - Rails Webook
Railsでは、ログイン時のメッセージや、登録や更新時の成功通知などの簡易な通知（フラッシュメッセージ）のために、flashという仕組みが用意されています。このflashの使い方を説明します。 (104KB)
Nov 27th, 2014 at 8:01 PM

new messages
[4:49]  
><%= yield %>は、どこコントローラの処理結果をうめこんでいるのでしょうか？
各アクション用のテンプレートを埋め込んで、HTML全体を出力します。
`xxx_controller.rb` の `yyyアクション` のデフォルトで選ばれるビュー（テンプレート）は、 `app/views/xxx/yyy.html.erb`になります。
ざっくり言えば「app/views/コントローラー名/アクション名.html.erb」ですね。
これがビューの共通ファイルであるレイアウトテンプレート（ app/views/layouts/application.html.erb  ）の `<%= yield %>` に埋め込まれて、HTML全体が出力されます。

>noticeは予約語で特別な変数です。フラッシュとして使います。
なるほど、別の所（flashオブジェクト）で定義されているのを、noticeが使用しているのですね。

>>><%= yield %>は、どこコントローラの処理結果をうめこんでいるのでしょうか？
各アクション用のテンプレートを埋め込んで、HTML全体を出力します。
`xxx_controller.rb` の `yyyアクション` のデフォルトで選ばれるビュー（テンプレート）は、 `app/views/xxx/yyy.html.erb`になります。
ざっくり言えば「app/views/コントローラー名/アクション名.html.erb」ですね。
これがビューの共通ファイルであるレイアウトテンプレート（ app/views/layouts/application.html.erb  ）の `<%= yield %>` に埋め込まれて、HTML全体が出力されます。


>`xxx_controller.rb` の `yyyアクション` のデフォルトで選ばれるビュー（テンプレート）は、 `app/views/xxx/yyy.html.erb`になります。

>これがビューの共通ファイルであるレイアウトテンプレート（ app/views/layouts/application.html.erb  ）の `<%= yield %>` に埋め込まれて、HTML全体が出力されます。
レイアウトテンプレート（ app/views/layouts/application.html.erb  ）が、どの画面でも表示されるもの（Message Borad）とポップアップ（<%= notice %> ）の実装をしているですね。「<%= yield %>」で、各アクションごとのHTMLファイルに処理結果を埋め込んだものを表示させているですね。

## controller
xxx_controller.rb messages_controller.rb

## action
- index
- create
- update
- destroy


/home/ubuntu/workspace/message-board/app/views

.
├── layouts
│   └── application.html.erb
├── message
│   ├── index.html.erb
│   └── index.html.erb_20170317
└── messages
    ├── _form.html.erb
    ├── edit.html.erb
    ├── index.html.erb
    └── index.html.erb_bk



## Herokuの注意点

```
>Herokuのフリープランではアクセスが30分間ない場合は、アプリがスリープしてRailsが起動していない状態になります。スリープ状態になっても再びアクセスすればRailsは起動します。もちろんスリープ状態から起動するので、アプリが表示されるまで少し時間がかかります。
herokuを停止状態するコマンドが見つかりませんでした。
注意点から自動的にスリープするから、コマンド自体が不要であり、コマンドがということですか？
