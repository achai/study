## 1.2 Ruby on Railsとは
framwork??

```
フトウェアフレームワーク（英: software framework）とは、プログラミングにおいて、一般的な機能をもつ共通コードをユーザーが選択的に上書きしたり特化させたりすることで、ある特定の機能をもたせようとする抽象概念のことである。単にフレームワークとも呼ばれる。
```
## 1.4 MVCパターン（Model - View - Controller パターン）

explan each patern(model,view,controller)
http://www.rubylife.jp/rails/ini/index7.html

# 2. deploy application
## 2.1 install rails

- what is rails command??
check below url.
http://guides.rubyonrails.org/command_line.html

# execute rails new command


```
# create rails application
rails new message-board
rails new message-board
      create  
      create  README.rdoc
      create  Rakefile
      create  config.ru
      create  .gitignore
      create  Gemfile
      create  app
      create  app/assets/javascripts/application.js
      create  app/assets/stylesheets/application.css
      create  app/controllers/application_controller.rb
      create  app/helpers/application_helper.rb
      create  app/views/layouts/application.html.erb
      create  app/assets/images/.keep
      create  app/mailers/.keep
      create  app/models/.keep
      create  app/controllers/concerns/.keep
      create  app/models/concerns/.keep
      create  bin
      create  bin/bundle
      create  bin/rails
      create  bin/rake
      create  bin/setup
      create  config
      create  config/routes.rb
      create  config/application.rb
      create  config/environment.rb
      create  config/secrets.yml
      create  config/environments
      create  config/environments/development.rb
      create  config/environments/production.rb
      create  config/environments/test.rb
      create  config/initializers
      create  config/initializers/assets.rb
      create  config/initializers/backtrace_silencers.rb
      create  config/initializers/cookies_serializer.rb
      create  config/initializers/filter_parameter_logging.rb
      create  config/initializers/inflections.rb
      create  config/initializers/mime_types.rb
      create  config/initializers/session_store.rb
      create  config/initializers/to_time_preserves_timezone.rb
      create  config/initializers/wrap_parameters.rb
      create  config/locales
      create  config/locales/en.yml
      create  config/boot.rb
      create  config/database.yml
      create  db
      create  db/seeds.rb
      create  lib
      create  lib/tasks
      create  lib/tasks/.keep
      create  lib/assets
      create  lib/assets/.keep
      create  log
      create  log/.keep
      create  public
      create  public/404.html
      create  public/422.html
      create  public/500.html
      create  public/favicon.ico
      create  public/robots.txt
      create  test/fixtures
      create  test/fixtures/.keep
      create  test/controllers
      create  test/controllers/.keep
      create  test/mailers
      create  test/mailers/.keep
      create  test/models
      create  test/models/.keep
      create  test/helpers
      create  test/helpers/.keep
      create  test/integration
      create  test/integration/.keep
      create  test/test_helper.rb
      create  tmp/cache
      create  tmp/cache/assets
      create  vendor/assets/javascripts
      create  vendor/assets/javascripts/.keep
      create  vendor/assets/stylesheets
      create  vendor/assets/stylesheets/.keep
         run  bundle install
```

## cution!!

Rails will set you up with what seems like a huge amount of stuff for such a tiny command! You've got the entire Rails directory structure now with all the code you need to run our simple application right out of the box.

## 2.3 サーバの起動(launch web server)


```
# start up server
achai3:~/workspace/message-board $ rails s -p $PORT -b $IP
=> Booting WEBrick
=> Rails 4.2.8 application starting in development on http://0.0.0.0:8080
=> Run `rails server -h` for more startup options
=> Ctrl-C to shutdown server
# normal web log??
[2017-03-12 22:47:50] INFO  WEBrick 1.3.1
[2017-03-12 22:47:50] INFO  ruby 2.3.0 (2015-12-25) [x86_64-linux]

# port number and process number by web server
[2017-03-12 22:47:50] INFO  WEBrick::HTTPServer#start: pid=44477 port=8080

# accesss logs
Started GET "/" for 106.181.132.31 at 2017-03-12 22:47:57 +0000
Cannot render console from 106.181.132.31! Allowed networks: 127.0.0.1, ::1, 127.0.0.0/127.255.255.255
Processing by Rails::WelcomeController#index as HTML
  Rendered /usr/local/rvm/gems/ruby-2.3.0/gems/railties-4.2.8/lib/rails/templates/rails/welcome/index.html.erb (1.7ms)
Completed 200 OK in 18ms (Views: 9.2ms | ActiveRecord: 0.0ms)
```

access web server to check rails application
https://techacademy-achai3.c9users.io/

## 2.5 サーバの終了(stop server)


## 2.6 Gitリポジトリの初期化(initialize git repository)
configure git,add stageing erea,commit local repository.

```
git init
git add *
git commit -m "first commit"
```

# 3. コントローラの作成(create controller)
## 3.1 MessagesControllerの作成(create message controller)


```
achai3:~/workspace/message-board (master) $ rails generate controller messages index
Running via Spring preloader in process 15588
Expected string default value for '--helper'; got true (boolean)
Expected string default value for '--assets'; got true (boolean)
   identical  app/controllers/messages_controller.rb
       route  get 'messages/index'
      invoke  erb
       exist    app/views/messages
   identical    app/views/messages/index.html.erb
      invoke  test_unit
   identical    test/controllers/messages_controller_test.rb
      invoke  helper
   identical    app/helpers/messages_helper.rb
      invoke    test_unit
      invoke  assets
      invoke    coffee
   identical      app/assets/javascripts/messages.coffee
      invoke    scss
   identical      app/assets/stylesheets/messages.scss
```

app/controllers/messages_controller.rb(controoler's file content)
inherit super class(ApplicationController)
```
class MessagesController < ApplicationController
  def index
  end
end
```

(view file content)
```
<h1>Messages#index</h1>
<p>Find me in app/views/messages/index.html.erb</p>
```

## 3.2 config/routes.rbの設定（ルーティング）(config and routes.rb setting(routeing))

# 4. モデルの作成(create model)
https://message-board-tecyacademy.c9.io

## 5.4 フォームの作成(create form)
- directory structure MVC model??

## 5.5. コントローラーの実装（controller ）
whne it creates new messages,display forms.

```
class MessagesController < ApplicationController
  def index
    @message = Message.new
  end

  def create
    @message = Message.new(message_params)
    @message.save
    redirect_to root_path , notice: 'メッセージを保存しました'
  end

  private
  def message_params
    params.require(:message).permit(:name, :body)
  end
  ## ここまで
end
```

> ルートURLにリダイレクトしています。
uhhn,i do not understnad what is redirection.

```
リダイレクトとは、別のURLを呼び出す処理をクライアントから出す要求。
```
http://www.rubylife.jp/rails/controller/index5.html


>params は、Ruby のハッシュの機能を拡張したクラスのオブジェクト
i forget hash.what is hash.

>
- フォームからのパラメータ
```
like arrgumment.but comblition key and value.
```


# 6. Bootstrapを使ったデザイン
## 6.1 レイアウトファイルの修正
## 6.2 メッセージの投稿・一覧画面のデザイン

# 7. バリデーションの実装(create validation)

## 7.1 バリデーションの書き方

```
# format
validates filed_name,condtion
```

app/models/message.rbって、今までなんで記述がないんだ？？

## 7.2 コントローラの修正(modify controller)

createメソッドにバリデーションを追加する。
条件式の判定は、「@message.save」返り値で行っている？？

```

class MessagesController < ApplicationController
  # 中略
  def create
    @message = Message.new(message_params)
    if @message.save
      redirect_to root_path , notice: 'メッセージを保存しました'
    else
      # メッセージが保存できなかった時
      @messages = Message.all
      flash.now[:alert] = "メッセージの保存に失敗しました。"
      render 'index'
    end
  end
  # 中略

  ```


8.1 編集機能の実装

> exceptのオプションでは、index,newアクションを作成しないように設定してます。
うん、アクションは起こせないようにするのはわかった。けど、なんだけえ？？

```
def index
  @message = Message.new
  #get all messages
  @messages = Message.all
 end

 ```
controlers method(new) is no description.

## 8.2 部分テンプレートの作成 ( create  part of templete)


### what is パーシャル

「_(レンダー名)」を指定するとテンプレート（モジュール化？）したものを読み込む。
```
<%= render 'form' %>
``
#### reference URL
http://qiita.com/shizuma/items/1c655dadd2e04b3990a8


8.3 編集画面の作成
編集画面へのリンクをつける。
app/views/messages/index.html.erb




## 8.5 フラッシュメッセージ
```
<html>
  <!-- 中略 -->
  <div class="container">
    <% if notice %> # noticeが正（何か表示するものがある場合）
      <div class="alert alert-info"> #ここはただのCSSを読み込んでいる
        <%= notice %> # noticeの内容を表示しているのか？
      </div>
    <% end %> # if条件式の範囲

    <%= yield %> # コントローラーの処理結果を埋め込む。どの処理を埋め込んでいるのか？
  </div>
</body>
</html>
```

8.6 削除機能の実装


9. Herokuで動作させる

gem file's content.

## database settening
explain config setteings.
http://www.rubylife.jp/rails/model/index2.html

```
〜略〜
production:(enviroment)
  <<: *default
  adapter: postgresql #type of database(データベースの種類)
  encoding: unicode # character code(文字コード)
  pool: 5
```

9.3 Herokuのアプリケーション作成
>Heroku上にアプリケーションを配置するには、Herokuでアプリケーションを作成し、gitでHerokuが用意したGitリポジトリに対してプッシュすることによりデプロイが行えます。
「herokuというレポジトリはない」というエラーが発生しました。リモートレポジトリを登録するのが抜けているではないですか？
git remote add heroku (herokuのリモートレポジトリ)

レポジトリはgit,herokuごとに必要？？それごとにgit操作が必要になるのか？？

## Herokuにアプリケーションをデプロイする
リモートレポジトリの登録が必要？か確認する。そんなレポジトリはないよって怒られる。。。

push時のログ
```
achai3:~/workspace/message-board/config (master) $ git push heroku master
Counting objects: 438, done.
Delta compression using up to 8 threads.
Compressing objects: 100% (361/361), done.
Writing objects: 100% (438/438), 372.13 KiB | 0 bytes/s, done.
Total 438 (delta 117), reused 0 (delta 0)
remote: Compressing source files... done.
remote: Building source:
remote:
remote: -----> Ruby app detected
remote: -----> Compiling Ruby/Rails
remote: -----> Using Ruby version: ruby-2.2.6
remote: -----> Installing dependencies using bundler 1.13.7
remote:        Running: bundle install --without development:test --path vendor/bundle --binstubs vendor/bundle/bin -j4 --deployment
remote:        Warning: the running version of Bundler (1.13.7) is older than the version that created the lockfile (1.14.6). We suggest you upgrade to the latest version of Bundler by running `gem install bundler`.
remote:        Fetching gem metadata from https://rubygems.org/..........
remote:        Fetching version metadata from https://rubygems.org/..
remote:        Fetching dependency metadata from https://rubygems.org/.
remote:        Installing minitest 5.10.1
remote:        Installing i18n 0.8.1
remote:        Installing rake 12.0.0
remote:        Installing thread_safe 0.3.6
remote:        Installing builder 3.2.3
remote:        Installing erubis 2.7.0
remote:        Installing mini_portile2 2.1.0
remote:        Installing rack 1.6.5
remote:        Installing mime-types-data 3.2016.0521
remote:        Installing arel 6.0.4
remote:        Installing coffee-script-source 1.12.2
remote:        Installing execjs 2.7.0
remote:        Installing thor 0.19.4
remote:        Installing concurrent-ruby 1.0.5
remote:        Installing multi_json 1.12.1
remote:        Using bundler 1.13.7
remote:        Installing json 1.8.6 with native extensions
remote:        Installing rdoc 4.3.0
remote:        Installing sass 3.4.23
remote:        Installing tilt 2.0.6
remote:        Installing sqlite3 1.3.13 with native extensions
remote:        Installing turbolinks-source 5.0.0
remote:        Installing tzinfo 1.2.2
remote:        Installing nokogiri 1.7.0.1 with native extensions
remote:        Installing mime-types 3.1
remote:        Installing rack-test 0.6.3
remote:        Installing coffee-script 2.4.1
remote:        Installing uglifier 3.1.7
remote:        Installing sprockets 3.7.1
remote:        Installing turbolinks 5.0.1
remote:        Installing activesupport 4.2.8
remote:        Gem::Ext::BuildError: ERROR: Failed to build gem native extension.
remote:        /tmp/build_8fdd952fc2addc44449daa230df9abfc/vendor/ruby-2.2.6/bin/ruby -r
remote:        ./siteconf20170320-222-192lf7s.rb extconf.rb
remote:        checking for sqlite3.h... no
remote:        sqlite3.h is missing. Try 'brew install sqlite3',
remote:        'yum install sqlite-devel' or 'apt-get install libsqlite3-dev'
remote:        and check your shared library search path (the
remote:        location where your sqlite3 shared library is located).
remote:        *** extconf.rb failed ***
remote:        Could not create Makefile due to some reason, probably lack of necessary
remote:        libraries and/or headers.  Check the mkmf.log file for more details.  You may
remote:        need configuration options.
remote:        Provided configuration options:
remote:        --with-opt-dir
remote:        --without-opt-dir
remote:        --with-opt-include
remote:        --without-opt-include=${opt-dir}/include
remote:        --with-opt-lib
remote:        --without-opt-lib=${opt-dir}/lib
remote:        --with-make-prog
remote:        --without-make-prog
remote:        --srcdir=.
remote:        --curdir
remote:        --ruby=/tmp/build_8fdd952fc2addc44449daa230df9abfc/vendor/ruby-2.2.6/bin/$(RUBY_BASE_NAME)
remote:        --with-sqlite3-config
remote:        --without-sqlite3-config
remote:        --with-pkg-config
remote:        --without-pkg-config
remote:        --with-sqlite3-dir
remote:        --without-sqlite3-dir
remote:        --with-sqlite3-include
remote:        --without-sqlite3-include=${sqlite3-dir}/include
remote:        --with-sqlite3-lib
remote:        --without-sqlite3-lib=${sqlite3-dir}/lib
remote:        extconf failed, exit code 1
remote:        Gem files will remain installed in
remote:        /tmp/build_8fdd952fc2addc44449daa230df9abfc/vendor/bundle/ruby/2.2.0/gems/sqlite3-1.3.13
remote:        for inspection.
remote:        Results logged to
remote:        /tmp/build_8fdd952fc2addc44449daa230df9abfc/vendor/bundle/ruby/2.2.0/extensions/x86_64-linux/2.2.0-static/sqlite3-1.3.13/gem_make.out
remote:        An error occurred while installing sqlite3 (1.3.13), and Bundler cannot
remote:        continue.
remote:        Make sure that `gem install sqlite3 -v '1.3.13'` succeeds before bundling.
remote:        Bundler Output: Warning: the running version of Bundler (1.13.7) is older than the version that created the lockfile (1.14.6). We suggest you upgrade to the latest version of Bundler by running `gem install bundler`.
remote:        Fetching gem metadata from https://rubygems.org/..........
remote:        Fetching version metadata from https://rubygems.org/..
remote:        Fetching dependency metadata from https://rubygems.org/.
remote:        Installing minitest 5.10.1
remote:        Installing i18n 0.8.1
remote:        Installing rake 12.0.0
remote:        Installing thread_safe 0.3.6
remote:        Installing builder 3.2.3
remote:        Installing erubis 2.7.0
remote:        Installing mini_portile2 2.1.0
remote:        Installing rack 1.6.5
remote:        Installing mime-types-data 3.2016.0521
remote:        Installing arel 6.0.4
remote:        Installing coffee-script-source 1.12.2
remote:        Installing execjs 2.7.0
remote:        Installing thor 0.19.4
remote:        Installing concurrent-ruby 1.0.5
remote:        Installing multi_json 1.12.1
remote:        Using bundler 1.13.7
remote:        Installing json 1.8.6 with native extensions
remote:        Installing rdoc 4.3.0
remote:        Installing sass 3.4.23
remote:        Installing tilt 2.0.6
remote:        Installing sqlite3 1.3.13 with native extensions
remote:        Installing turbolinks-source 5.0.0
remote:        Installing tzinfo 1.2.2
remote:        Installing nokogiri 1.7.0.1 with native extensions
remote:        Installing mime-types 3.1
remote:        Installing rack-test 0.6.3
remote:        Installing coffee-script 2.4.1
remote:        Installing uglifier 3.1.7
remote:        Installing sprockets 3.7.1
remote:        Installing turbolinks 5.0.1
remote:        Installing activesupport 4.2.8
remote:        Gem::Ext::BuildError: ERROR: Failed to build gem native extension.
remote:        
remote:        /tmp/build_8fdd952fc2addc44449daa230df9abfc/vendor/ruby-2.2.6/bin/ruby -r
remote:        ./siteconf20170320-222-192lf7s.rb extconf.rb
remote:        checking for sqlite3.h... no
remote:        sqlite3.h is missing. Try 'brew install sqlite3',
remote:        'yum install sqlite-devel' or 'apt-get install libsqlite3-dev'
remote:        and check your shared library search path (the
remote:        location where your sqlite3 shared library is located).
remote:        *** extconf.rb failed ***
remote:        Could not create Makefile due to some reason, probably lack of necessary
remote:        libraries and/or headers.  Check the mkmf.log file for more details.  You may
remote:        need configuration options.
remote:        
remote:        Provided configuration options:
remote:        --with-opt-dir
remote:        --without-opt-dir
remote:        --with-opt-include
remote:        --without-opt-include=${opt-dir}/include
remote:        --with-opt-lib
remote:        --without-opt-lib=${opt-dir}/lib
remote:        --with-make-prog
remote:        --without-make-prog
remote:        --srcdir=.
remote:        --curdir
remote:        --ruby=/tmp/build_8fdd952fc2addc44449daa230df9abfc/vendor/ruby-2.2.6/bin/$(RUBY_BASE_NAME)
remote:        --with-sqlite3-config
remote:        --without-sqlite3-config
remote:        --with-pkg-config
remote:        --without-pkg-config
remote:        --with-sqlite3-dir
remote:        --without-sqlite3-dir
remote:        --with-sqlite3-include
remote:        --without-sqlite3-include=${sqlite3-dir}/include
remote:        --with-sqlite3-lib
remote:        --without-sqlite3-lib=${sqlite3-dir}/lib
remote:        
remote:        extconf failed, exit code 1
remote:        
remote:        Gem files will remain installed in
remote:        /tmp/build_8fdd952fc2addc44449daa230df9abfc/vendor/bundle/ruby/2.2.0/gems/sqlite3-1.3.13
remote:        for inspection.
remote:        Results logged to
remote:        /tmp/build_8fdd952fc2addc44449daa230df9abfc/vendor/bundle/ruby/2.2.0/extensions/x86_64-linux/2.2.0-static/sqlite3-1.3.13/gem_make.out
remote:        
remote:        An error occurred while installing sqlite3 (1.3.13), and Bundler cannot
remote:        continue.
remote:        Make sure that `gem install sqlite3 -v '1.3.13'` succeeds before bundling.
remote:  !
remote:  !     Failed to install gems via Bundler.
remote:  !     Detected sqlite3 gem which is not supported on Heroku:
remote:  !     https://devcenter.heroku.com/articles/sqlite3
remote:  !
remote:  !     Push rejected, failed to compile Ruby app.
remote:
remote:  !     Push failed
remote: Verifying deploy...
remote:
remote: !       Push rejected to asai-message-board.
remote:
To https://git.heroku.com/asai-message-board.git
 ! [remote rejected] master -> master (pre-receive hook declined)
error: failed to push some refs to 'https://git.heroku.com/asai-message-board.git'
```





```
NoMethodError in Users#new
```

no define method.

## 6.7.user detail display

### what is ruby helper function??
http://guides.rubyonrails.org/form_helpers.html

Forms are essential interface for user input.

## 6.8.add flash messages()
easy to display message.
howt use flash.

http://railsdoc.com/references/flash

app/controllers/users_controller.rb
```

  if user.save
    flash[:successs] = "Welecom to the sample APP"
    return?to @user
 else

 end
```


each message_type result outputs html file..
```

<div class="cocntainser">
  <% flash.each do |message_type, message| %>
    <div class="alert alert-<%= message_type %>"><%= message %></div>"
  <% end %>
```

# 7. login function
## 7.1. session
server and browser has session.
destory session.

## 7.2.create controller and router

controller name : session
action : new
```
rails generate controller session new
```

- display login page
- create new user
- leave web page

config/routes.rb
```
get 'login' to: 'session#new'
post 'login' to: 'session#create'
delete 'logout' to: 'session#destory'
```

## 7.3. create view

## 7.4. createa controller

app/controllers/sessions_controller.rb
```
class SessionCntroller < ApplicationController
  def new
  end

  def create
    @user = User.find_by(:email, params[:session][:email].downcase)
    if @user && @user.authtenticate(params[session][:password])
      session[:user_id] = @user.id
      flash[:info] = "login in user #{@user.name}"
      redirect_to @user
    else
      flash[:danger] = 'invalid email or password combination'
      render new
    end
  end

  def destory #セッションを破棄した場合はいつでもこれを呼ぶ。ホームを返す。
    session[:id] = nil
    redirect_to root_path
  end

  ## 7.5 createa helper methods

  app/helper/session_helper.rb
  ```
module SessionHelper
 def current_user
  @current_user ||= User.find_by(id:, session[:user_id])
 end
 def logged_in?
  !!current_user
 end
 def store_location
  session[:forward_url] = request.url if reqest.get?
 end
  ```

app/controllers/application_controller.rb
```

private
  def logged_in_user
    unless logged_in?
      store_location
      flash[:danger] = "Please log in"
      redirect_to login_url
    end
  end
```
