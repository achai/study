# 2.2 レスポンシブ・ウェブデザインに対応
## 質問
HTML file ready for each device(PC,smart phone,tablet).
how to apply cascading style sheet?

## ANSWER
as described below in detailed.(下記に詳細に記載されている。)
http://www.coprosystem.co.jp/tipsblog/2013/06/28.html
I am ready same html file for each device.（自分で書いたやつ。各ディバイスごとにファイルが必要だよ）
It is necessary to prepare html file for each device.（グーグル先生）
- HTML
  - computer
    - index
  - smartphone
  - other
_ CSS
  - test.css(respnsive webdesign)


# 7.2 サンプルコード

form.html
```
<form>
  <div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
  </div>
  <div class="form-group">
    <label for="exampleInputFile">File input</label>
    <input type="file" id="exampleInputFile">
    <p class="help-block">Example block-level help text here.</p>
  </div>
  <div class="checkbox">
    <label>
      <input type="checkbox"> Check me out
    </label>
  </div>
  <button type="submit" class="btn btn-default">Submit</button>
</form>
```
## ENGLISH
CSSを適用する。
⇒apply cascading style sheet.
