#!/bin/bash

#refresh node cache
acdcli sync

#export lang
export LC_CTYPE=ja_JP.UTF-8

# set_variable
local_directory="/Users/asaitakayuki/Downloads/amazon_drive_sync/"
day=`date +"%Y/%m/%d"`
backup_file_name=manga_backup_$day

#change directory to local_directory
cd $local_directory
echo `pwd`

#upload manga direcotry
acdcli upload /Users/asaitakayuki/Downloads/amazon_drive_sync/manga/ / 
acdcli upload /Users/asaitakayuki/Downloads/amazon_drive_sync/photo /
acdcli upload /Users/asaitakayuki/Downloads/amazon_drive_sync/reference /
