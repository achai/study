function UpdateManagementFiles() {
  /* 概要：申請者から新規ゲスト追加申請のメールが来た場合、管理簿に申請内容を追加し、返信用メールを下書き作成します。*/

  //メールの検索条件（決まった件名の命名規則がないので、検索でヒットさせる条件が難しい。。。。
  //var strTerms = '("filename:ログ保管設定ワークシート is:unread label:inbox ")';
  var strTerms = '(label:inbox is:unread filename:ログ保管設定ワークシート)';

  var myThreads = GmailApp.search(strTerms, 0, 10); //条件にマッチしたスレッドを取得
  var myMsgs = GmailApp.getMessagesForThreads(myThreads); //スレッドからメールを取得する　→二次元配列で格納
  var getActiveSheet = SpreadsheetApp.getActiveSpreadsheet();
  var getFTPManagementSheet = getActiveSheet.getSheetByName("管理簿");
  var getSyslogManagementSheet = getActiveSheet.getSheetByName("管理簿_syslog");
  var MailTempelteSheet = getActiveSheet.getSheetByName("mail_templete");
  var valMsgs = [];

  /各メールから日時、送信元、件名、内容を取り出す/
  for(var i = 0;i < myMsgs.length;i++){

    valMsgs[i] = [];

    //件名を取得。
    valMsgs[i][1] = myMsgs[i][0].getSubject();

    //件名にsyslogが含むか判定
    if( valMsgs[i][1].indexOf('syslog') != -1 ){
      //シート管理簿_syslogの最終行に追加する。
      getSyslogManagementSheet.appendRow([myMsgs[i][0].getDate(), myMsgs[i][0].getSubject()]);

      //mail_templeteからメールの記載内容を取得
      var mail_context = MailTempelteSheet.getRange('B11');

    //ログ保管基盤利用申請メールに申請受付完了メールを作成
      var myMsgs = GmailApp.getMessagesForThreads(myThreads);
      myMsgs[i][0].createDraftReplyAll(mail_context.getValue());

    } else {
      //管理簿最終行に追記する。
      getFTPManagementSheet.appendRow([myMsgs[i][0].getDate(), myMsgs[i][0].getSubject(),  "", "", "", "","未着手","未着手","未依頼"]);

      //mail_templeteからメールの記載内容を取得
      var mail_context = MailTempelteSheet.getRange('B3');

      //ログ保管基盤利用申請メールに申請受付完了メールを作成
      var myMsgs = GmailApp.getMessagesForThreads(myThreads);
      myMsgs[i][0].createDraftReplyAll(mail_context.getValue());
    }

    //既読にする
      myThreads[i].markRead();

  }
}
