function delete_unread_mail() {
/*
概要：毎日、未読メールを削除する。
トリガー：午前０〜１時の間
*/
  var threads = GmailApp.search('in:inbox is:unread');

  for( var i = 0; i <= threads.lentgh;i++){
    threads[i].moveToTrash();
  }
}