for i in range(1,10000):
    if i % 15 == 0:
        print "BuzzFizz" 
    elif i % 5 == 0:
        print "Fizz"     
    elif i % 3 == 0:
        print "Buzz"        
    else:
        print i
