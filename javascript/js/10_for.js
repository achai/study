/*
for文（whileに比べて１つで書ける。）
共通して使える命令がある。
    break;ループ終了
    continue;一回処理をスキップする。
    
*/

for(var i = 0;i<10;i++){
    if (i === 5){
        continue;
    }
    console.log(i);
}
