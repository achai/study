/*
whileの書き方

*/


//No.1
var i = 100;
/*
while(i<10){
    console.log(i);
    i++;
}
*/

//No.2
do {
    console.log(i);
    i++;
} while(i<10);